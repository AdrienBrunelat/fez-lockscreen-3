/* Global variable calls */
//var requestAnimationFrame = requestAnimationFrame || {};


var RenderersManager = {
	renderers: undefined,
	time: undefined,
    
    init: function (renderers) {
        'use strict';
        var renderersManager = this;
        
        this.renderers = renderers.reverse();
        requestAnimationFrame(function (now) {
            renderersManager.update(now);
            requestAnimationFrame(function (now) {
                renderersManager.frame();
            });
        });
    },
    
    frame: function (now) {
		'use strict';
        var len,
            i,
            manager = this;
        
		requestAnimationFrame(function (now) {
            manager.frame(now);
        });

        len = this.renderers.length;
		for (i = len; i--;) {
			this.renderers[i].render();
		}
        this.update(now);
    },
    
    update: function (now) {
		'use strict';
        var dt,
            n,
            len,
            i;
        
        
        // If dt is at 1s, use another version of update for the sleep case of iPhone
		dt = Math.min(1000, now - (this.time || now)) || 0;
        //Prevents animation to crash if time goes backwards.
        //Which in theory... Shouldn't be a problem.
        //Or, at least, if time starts to go backwards,
        //Lockscreen not animating won't be the most important issue on earth.
        //dt = dt >= 0 ? dt : 0;
        
		this.time = now;

        len = this.renderers.length;
        for (i = 0; i < len; i += 1) {
		//for (i = len; i--;) {
			this.renderers[i].update(dt);
		}
	}
};

//By Acemond

/* -------------------- Settings --------------------- */
//var Slider Text
var unlockText = "> Slide to unlock";

//var Clock Mode 12h
var mode12h = true; // true is 12h mode, false is 24h mode

//var Pixel font as default
var pixel = true; // Sets default font to Pixel font for text

//var Clock updates (ms)
var clockCheckInterval = 1000.0; //Interval for clock update in milliseconds

//var Clouds Animation
var movingClouds = true;

//var Clouds Speed
var cloudsSpeed = 2;

//var Cube Quality
var cubeScale = 2;

//var Signal Quality
var signalScale = 1;

//var Animation FPS
var fps = 50;

//var Debug Mode
var debug = false;
/* --------------------------------------------------- */

//var INTERNAL USE
var Config = undefined;
Config = {
    unlockText: unlockText,
    mode12h: mode12h,
    pixel: pixel,
    clockCheckInterval: clockCheckInterval,
    movingClouds: movingClouds,
    cubeScale: cubeScale,
    signalScale: signalScale,
    fps: fps,
    debug: debug,
    cloudsSpeed: cloudsSpeed
};

//By Acemond

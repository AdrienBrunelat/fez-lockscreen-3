var Ratio = Ratio || {},
    ImageManager = ImageManager || {},
    Clock = Clock || {};

var Gomez = {
    // Gomez state private variables
    StateEnum: {
        up: 0,
        sleeping: 1,
        countingSheeps: 2,
        wakingUp: 3,
        playing: 4,
        yawning: 5,
        lookingAround: 6,
        blinking: 7,
        jumping: 8,
        cubeJumping: 9
    },
    state: undefined,
    lastBlinkCounter: 0,
    lastIdleCounter: 0,
    lastIdleState: undefined,
    doubleBlink: false,
    
    // Rendering private variables
    windowHRatio: undefined,
    canvas: undefined,
    canvasWidth: undefined,
    canvasHeight: undefined,
    canvas2DContext: undefined,
    
    // Timing private variables
    gomezSchedule: undefined,
    sleepingTimeout: undefined,
    
    // Animation private vars
    animState: undefined,
	dt: 0,
	frame: 0,
	outdated: true,
	animated: undefined,
    
    // External classes references
    clock: undefined,
    
    // Gomez animation images
    gomezUpImage: undefined,
    sleepingImage: undefined,
    countingSheepsImage: undefined,
    wakingUpImage: undefined,
    playingImage: undefined,
    yawning: undefined,
    lookingAround: undefined,
    blinking: undefined,
    jumping: undefined,
    cubeJumping: undefined,
    
    init: function (canvas) {
        'use strict';
        var renderingWindow = window,
            windowHRatio = Ratio.horizontal,
            gomez = this,
            images = ImageManager.images;
        
        this.windowHRatio = windowHRatio;
        this.clock = Clock;
        
        renderingWindow.setInterval(
            function () {
                gomez.gomezIdle();
            },
            1000
        );
        
        this.gomezSchedule = renderingWindow.setInterval(
            function () {
                gomez.stateRequest(gomez.checkGomezSchedule());
            },
            1000
        );

        canvas.width = 105 * windowHRatio;
        canvas.height = 195 * windowHRatio;
        canvas.style.left = 100 * windowHRatio + 'px';
        canvas.style.bottom = ((ImageManager.images['floor.png'].height - 15) * 3 * windowHRatio) + 'px';

        this.state = this.checkGomezSchedule();
        canvas.addEventListener('click', function () {
            gomez.poke();
        });
        
        this.canvas = canvas;
        this.canvas2DContext = canvas.getContext('2d');
        this.canvas2DContext.webkitImageSmoothingEnabled = false;
        this.canvasWidth = 105 * windowHRatio;
        this.canvasHeight = 195 * windowHRatio;
        
        this.upImage = images['gomez_up.png'];
        this.sleepingImage = images['gomez_sleeping.png'];
        this.countingSheepsImage = images['gomez_countingSheeps.png'];
        this.wakingUpImage = images['gomez_wakingUp.png'];
        this.playingImage = images['gomez_playing.png'];
        this.yawningImage = images['gomez_yawning.png'];
        this.lookingAroundImage = images['gomez_lookingAround.png'];
        this.blinkingImage = images['gomez_blinking.png'];
        this.jumpingImage = images['gomez_jumping.png'];
        this.cubeJumpingImage = images['gomez_cube.png'];
    },
    
    poke: function () {
        'use strict';
        var gomez = this;
        
        window.clearInterval(this.gomezSchedule);
        window.clearTimeout(this.sleepingTimeout);

        this.sleepingTimeout = setTimeout(function () {
            gomez.gomezSchedule = window.setInterval(function () {
                gomez.stateRequest(gomez.checkGomezSchedule());
            }, 1000);
        }, 10000);

        if (this.state === this.StateEnum.sleeping) {
            gomez.stateRequest(gomez.StateEnum.up);
        } else {
            gomez.stateRequest(gomez.StateEnum.jumping);
        }
    },
    
    checkGomezSchedule: function () {
        'use strict';
        var currentTime = this.clock.currentTime,
            h = currentTime.getHours(),
            m = currentTime.getMinutes(),
            s = currentTime.getSeconds(),
            timeSeed = this.clock.timeSeed;

        if (((h === 6 && m > (timeSeed % 60)) || (h > 6)) &&
                ((h < 21) || (h === 21 && m < (60 - timeSeed % 60)))) {
            if (h <= 11 ||  h >= 14) {
                return this.StateEnum.up;
            }
            
            //Nap time randomization
            switch (timeSeed % 10) {
            case 2:
                if (h === 12 &&
                        m >= 10 &&
                        m <= 33 &&
                        (m > 10 || s >= 29)) {
                    return this.StateEnum.sleeping;
                } else {
                    return this.StateEnum.up;
                }
            case 3:
                if (h === 12 && m >= 22 && m <= 46 &&
                        (m > 22 || s >= 56)) {
                    return this.StateEnum.sleeping;
                } else {
                    return this.StateEnum.up;
                }
            case 4:
                if (((h === 12 && m >= 40 && m <= 59) ||
                        (h === 13 && m <= 4)) &&
                        (h === 13 || m > 40 || s >= 35)) {
                    return this.StateEnum.sleeping;
                } else {
                    return this.StateEnum.up;
                }
            case 5:
                if (h === 13 && m >= 12 && m <= 39 &&
                        (m > 12 || s >= 38)) {
                    return this.StateEnum.sleeping;
                } else {
                    return this.StateEnum.up;
                }
            case 6:
                if (h === 13 && m >= 38 && m <= 55 &&
                        (m > 38 || s >= 52)) {
                    return this.StateEnum.sleeping;
                } else {
                    return this.StateEnum.up;
                }
            case 7:
                if (h === 13 && m >= 20 && m <= 44 &&
                        (m > 20 || s >= 47)) {
                    return this.StateEnum.sleeping;
                } else {
                    return this.StateEnum.up;
                }
            default:
                return this.StateEnum.up;
            }
        
        } else {
            return this.StateEnum.sleeping;
        }
    },
    gomezIdle: function () {
        'use strict';
        var blink = false,
            anime = false,
            random = Math.random,
            floor = Math.floor,
            r = random(),
            temp;  // temp var used to simplify calculations
        
        //If user inputs a jump, it is considered as an animation
        if (this.state === this.StateEnum.jumping) {
            this.lastIdleCounter = 0;
            this.lastBlinkCounter = 4;
        }
        
        if (this.state === this.StateEnum.up) {
            this.lastBlinkCounter += 1;
            this.lastIdleCounter += 1;
            
            temp = 8 - this.lastBlinkCounter;
            
            if (this.doubleBlink) {
                this.doubleBlink = false;
            } else if (this.lastBlinkCounter <= 1) {
                if (floor(r * 8.0) === 0) {
                    blink = true;
                    this.doubleBlink = true;
                }
            } else if (this.lastBlinkCounter < 9) {
                temp = 9 - this.lastBlinkCounter;
                if (floor(r * temp * temp) === 0) {
                    blink = true;
                }
            } else if (floor(r * temp * temp) === 0) {
                blink = true;
            }
        }
        
        r = random();
        if (blink) {
            this.stateRequest(this.StateEnum.blinking);
            this.lastBlinkCounter = 0;
        } else if (this.state === this.StateEnum.up) {
            if (this.lastIdleCounter >= 6) {
                temp = 18 - this.lastIdleCounter;
                if (this.lastIdleCounter < 8) {
                    temp = 12 - this.lastIdleCounter;
                    if (floor(r * temp * temp) === 0) {
                        anime = true;
                    }
                } else if (this.lastIdleCounter < 19) {
                    temp = 10 - (this.lastIdleCounter / 2.0);
                    if (floor(r * temp * temp) === 0) {
                        anime = true;
                    }
                } else if (floor(r * temp * temp) === 0) {
                    anime = true;
                }
            }
        
            r = random();
            if (anime) {
                this.lastIdleCounter = 0;
                this.lastBlinkCounter = 4;
                
                if (this.lastIdleState === this.StateEnum.yawning) {
                    if (r <= 0.2) {
                        this.lastIdleState = this.StateEnum.yawning;
                    } else if (r >= 0.6) {
                        this.lastIdleState = this.StateEnum.playing;
                    } else {
                        this.lastIdleState = this.StateEnum.lookingAround;
                    }
                } else if (this.lastIdleState === this.StateEnum.playing) {
                    if (r <= 0.5) {
                        this.lastIdleState = this.StateEnum.yawning;
                    } else if (r >= 0.9) {
                        this.lastIdleState = this.StateEnum.playing;
                    } else {
                        this.lastIdleState = this.StateEnum.lookingAround;
                    }
                } else if (this.lastIdleState === this.StateEnum.lookingAround) {
                    if (r <= 0.5) {
                        this.lastIdleState = this.StateEnum.yawning;
                    } else if (r >= 0.6) {
                        this.lastIdleState = this.StateEnum.playing;
                    } else {
                        this.lastIdleState = "lookingAround";
                    }
                } else { //Initial state, this.lastIdleState=""
                    if (r <= 0.4) {
                        this.lastIdleState = this.StateEnum.yawning;
                    } else if (r >= 0.7) {
                        this.lastIdleState = this.StateEnum.playing;
                    } else {
                        this.lastIdleState = this.StateEnum.lookingAround;
                    }
                }
                this.stateRequest(this.lastIdleState);
            }
        }
    },
    stateRequest: function (wantedState) {
        'use strict';
        var gomez;
        
        if (this.state === wantedState) {
            return;
        }
        
        switch (wantedState) {
        case this.StateEnum.sleeping:
            if (this.state === this.StateEnum.sleeping ||
                    this.state === this.StateEnum.countingSheeps) {
                return;
            }
            if (this.state === this.StateEnum.up) {
                this.state = this.StateEnum.countingSheeps;
            } else if (this.state !== this.StateEnum.cubeJumping) {
                gomez = this;
                setTimeout(function () {
                    gomez.stateRequest(gomez.StateEnum.sleeping);
                }, 500);
            }
            break;
                
        case this.StateEnum.up:
            if (this.state === this.StateEnum.sleeping) {
                this.state = this.StateEnum.wakingUp;
            }
            break;
                
        case this.StateEnum.cubeJumping:
            if (this.state === this.StateEnum.sleeping) {
                this.state = this.StateEnum.cubeJumping;
            } else if (this.state === this.StateEnum.up) {
                this.state = this.StateEnum.cubeJumping;
            } else if (this.state !== this.StateEnum.cubeJumping) {
                gomez = this;
                setTimeout(function () {
                    gomez.stateRequest("cube");
                }, 500);
            }
            break;
                
        case this.StateEnum.blinking:
            if (this.state === this.StateEnum.up) {
                this.state = this.StateEnum.blinking;
            }
            break;
                
        case this.StateEnum.lookingAround:
            if (this.state === this.StateEnum.up) {
                this.state = this.StateEnum.lookingAround;
            }
            break;
                
        case this.StateEnum.yawning:
            if (this.state === this.StateEnum.up) {
                this.state = this.StateEnum.yawning;
            }
            break;
                
        case this.StateEnum.playing:
            if (this.state === this.StateEnum.up) {
                this.state = this.StateEnum.playing;
            }
            break;
                
        case this.StateEnum.jumping:
            if (this.state === this.StateEnum.up ||
                    this.state === this.StateEnum.blinking ||
                    this.state === 'idleAnimation') {
                this.state = this.StateEnum.jumping;
            }
            break;
                
        default:
            gomez = this;
            setTimeout(function () {
                gomez.stateRequest(wantedState);
            }, 500);
            break;
        }
    },
    
    update: function (dt) {
        'use strict';
        var n,
            fps = 100.0;
        
        if (this.state === this.animState && this.animated === false) {
            return;
        }

        if (this.animState === this.StateEnum.jumping) {
            fps = 50.0;
        }
        n = Math.floor((this.dt + dt) / fps);
        if (n === 0) {
            this.dt += dt;
            return;
        } else {
            this.dt = 0;
            this.frame += n;
            this.outdated = true;
        }
    },
    render: function () {
        'use strict';
        
        if (!this.outdated) {
            return;
        } else {
            this.outdated = false;
        }
        
        if (this.state !== this.animState) {
            this.frame = 0;
        }
    
        switch (this.state) {
        case this.StateEnum.sleeping:
            this.renderSleeping();
            break;
        case this.StateEnum.up:
            this.renderUp();
            break;
        case this.StateEnum.wakingUp:
            this.renderWakingUp();
            break;
        case this.StateEnum.countingSheeps:
            this.renderCountingSheeps();
            break;
        case this.StateEnum.cubeJumping:
            this.renderCubeJumping();
            break;
        case this.StateEnum.blinking:
            this.renderBlinking();
            break;
        case this.StateEnum.lookingAround:
            this.renderLookingAround();
            break;
        case this.StateEnum.yawning:
            this.renderYawning();
            break;
        case this.StateEnum.playing:
            this.renderPlaying();
            break;
        case this.StateEnum.jumping:
            this.renderJumping();
            break;
        default:
            this.renderUp();
            break;
        }
    },
    
    renderUp: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.upImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
	
		this.animated = false;
		this.animState = this.StateEnum.up;
		ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
		ctx.drawImage(gomezImage,
                          0,
                          0,
                          gomezImageWidth,
                          gomezImageHeight,
                          10 * hRatio,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          gomezImageWidth * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
		             );
	},
    renderSleeping: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.sleepingImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
    
        if (this.frame > 14) {
            this.frame = 14;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.sleeping;
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          19 * this.frame,
                          0,
                          19,
                          gomezImageHeight,
                          5 * hRatio,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          19 * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
                     );
        if (this.frame > 13) {
            this.frame = -1;
        }
    },
    renderCountingSheeps: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.countingSheepsImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
    
        if (this.frame > 15) {
            this.frame = 15;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.countingSheeps;
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          19 * this.frame,
                          0,
                          19,
                          gomezImageHeight,
                          5 * hRatio,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          19 * 5 * hRatio, gomezImageHeight * 5 * hRatio
                      );
        if (this.frame > 14) {
            this.state = this.StateEnum.sleeping;
            this.frame = -1;
        }
    },
    renderWakingUp: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.wakingUpImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
    
        if (this.frame > 11) {
            this.frame = 11;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.wakingUp;
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          19 * this.frame,
                          0,
                          19,
                          gomezImageHeight,
                          5 * hRatio,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          19 * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
                     );
        if (this.frame > 10) {
            this.state = this.StateEnum.up;
            this.frame = -1;
        }
    },
    renderPlaying: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.playingImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio,
            lastFrame = false;
    
        if (this.frame > 28) {
            this.frame = 28;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.playing;
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          16 * this.frame,
                          0,
                          16,
                          gomezImageHeight,
                          0,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          16 * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
                     );
        if (this.frame > 27) {
            this.state = this.StateEnum.up;
            this.frame = -1;
        }
    },
    renderYawning: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.yawningImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
    
        if (this.frame > 8) {
            this.frame = 8;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.yawning;
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          13 * this.frame,
                          0,
                          13,
                          gomezImageHeight,
                          10 * hRatio,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          13 * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
                     );
        if (this.frame > 7) {
            this.state = this.StateEnum.up;
            this.frame = -1;
        }
    },
    renderLookingAround: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.lookingAroundImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
    
        if (this.frame > 27) {
            this.frame = 27;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.lookingAround;
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          15 * this.frame,
                          0,
                          15, gomezImageHeight,
                          0,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          15 * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
                     );
        if (this.frame > 26) {
            this.state = this.StateEnum.up;
        }
    },
    renderBlinking: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.blinkingImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
    
        if (this.frame > 1) {
            this.frame = 1;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.blinking;
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          11 * this.frame,
                          0,
                          11,
                          gomezImageHeight,
                          20 * hRatio,
                          this.canvasHeight - (gomezImageHeight + 10) * 5 * hRatio,
                          11 * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
                     );
        if (this.frame > 0) {
            this.state = this.StateEnum.up;
        }
    },
    renderJumping: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.jumpingImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
    
        if (this.frame > 20) {
            this.frame = 20;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.jumping;
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          13 * this.frame,
                          0,
                          13,
                          gomezImageHeight,
                          10 * hRatio,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          13 * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
                     );
        if (this.frame > 19) {
            this.state = this.StateEnum.up;
            this.frame = -1;
        }
    },
    renderCubeJumping: function () {
        'use strict';
		var c = this.canvas,
			ctx = this.canvas2DContext,
            gomezImage = this.cubeJumpingImage,
            gomezImageWidth = gomezImage.width,
            gomezImageHeight = gomezImage.height,
            hRatio = this.windowHRatio;
    
        if (this.frame > 14) {
            this.frame = 14;
        }
        
        this.animated = true;
        this.animState = this.StateEnum.cubeJumping;
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(gomezImage,
                          13 * this.frame,
                          0,
                          13,
                          gomezImageHeight,
                          5 * hRatio,
                          this.canvasHeight - gomezImageHeight * 5 * hRatio,
                          13 * 5 * hRatio,
                          gomezImageHeight * 5 * hRatio
                     );
        if (this.frame > 13) {
            this.animated = false;
            this.frame = -1;
        }
    }
};






















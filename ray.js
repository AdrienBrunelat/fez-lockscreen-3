var ImageManager = ImageManager || {},
    Ratio = Ratio || {},
    Environment = Environment || {};

var Ray = function (color, ctx, x0, y0, x1, w) {
    'use strict';
    var r = color[0],
        g = color[1],
        b = color[2];

    this.hRatio = Ratio.horizontal;

    this.canvas2DContext = ctx;
    this.cloudsHeight = [170, 130, 60, 0];

    this.color = color;  // color should be formatted like: [r, g, b]
    this.x = x0;
    this.y = y0;
    this.z = Math.floor(Math.random() * 4);
    this.width = w;

    this.startXPos = x0;
    this.endXPos = x1;

    this.frame = 0;
    this.opacity = 0;
    this.maxOpacity = Math.random() * 0.5 + 0.5;
    this.over = false;

    this.gradient = ctx.createLinearGradient(
        0,
        0,
        0,
        (this.cloudsHeight[this.z] + 51 + Math.floor(Math.random() * 150)) * this.hRatio
    );

    this.gradient.addColorStop(0, "rgba(" + r + ", " + g + ", " + b + ", 1)");
    this.gradient.addColorStop(0.9, "rgba(" + r + ", " + g + ", " + b + ", 0)");

    this.update = function (dt) {
        var distance = this.endXPos - this.startXPos,
            diff;

        this.x += (distance / 200);

        diff = Math.abs(this.endXPos - this.x);
        if (diff > Math.abs(distance) * 4 / 5) {
            this.opacity += 0.025;
            this.opacity = Math.min(this.maxOpacity, this.opacity);
        } else if (diff < Math.abs(distance) / 5) {
            this.opacity -= 0.025;
            this.opacity = Math.max(0, this.opacity);
        }

        if (this.x >= this.endXPos) {
            this.over = true;
        }
    };

    this.render = function () {
        var ctx = this.canvas2DContext;

        ctx.beginPath();
        ctx.moveTo(this.x, this.y - this.width);
        ctx.lineTo(this.x - 370 * this.hRatio, this.y + 370 * this.hRatio - this.width);
        ctx.closePath();

        ctx.lineWidth = this.width;
        ctx.strokeStyle = this.gradient;

        ctx.globalAlpha = this.opacity;
        ctx.stroke();
        ctx.globalAlpha = 1;
    };
};

var Sun = {
    window: window,
    windowInnerWidth: undefined,
    windowHRatio: undefined,
    windowVRatio: undefined,

    canvas: undefined,
    canvas2DContext: undefined,

    init: function (canvas) {
        'use strict';

        this.canvas = canvas;
        this.canvas2DContext = canvas.getContext('2d');

        this.windowInnerWidth = this.window.innerWidth;

        this.windowHRatio = Ratio.horizontal;
        this.windowVRatio = Ratio.vertical;
    },

    castNewRay: function (color) {
        'use strict';
        var ray,
            // Here, 370 are added to windowInnerWidth value to centralize rays a bit more
            x0 = Math.random() * (this.windowInnerWidth + 370 * this.windowHRatio),
            x1 = x0 + ((Math.random() * 100) + 10) * this.windowHRatio;

        return new Ray(
            color,
            this.canvas2DContext,
            x0,
            0,
            x1,
            (Math.floor(Math.random() * 75) + 10) * this.windowHRatio
        );
    }
};
/* Global variables call */
var Config = Config || {},
    Environment = Environment || {},
    Clock = Clock || {},
    CubeShard = CubeShard || {},
    RenderersManager = RenderersManager || {},
    Gomez = Gomez || {},
    Sun = Sun || {},
    Clouds = Clouds || {},
    Signal = Signal || {},
    ImageManager = ImageManager || {},
    Engine3D = Engine3D || {};

//window.addEventListener('load', window.start);

var Ratio = {
        horizontal: undefined,
        vertical: undefined
    };

function allImagesLoaded() {
	'use strict';
    var glCanvas,
        renderersManager,
        x,
        y,
        win = window,
        hRatio = Ratio.horizontal,
        images = ImageManager.images,
        floorHeight = images['floor.png'].height,
        setsWidth = images['sets.png'].width,
        setsHeight = images['sets.png'].height,
        cubeScale = Config.cubeScale,
        random = Math.random,
        webGLRenderer,
        doc = document;
    
    // TODO: check if working properly
    doc.removeEventListener('allImagesLoaded', function () {
        allImagesLoaded(ImageManager);
    });
    
	win.slideToUnlock.innerHTML = Config.unlockText + "</span>";
	win.slideToUnlock.style.bottom = (floorHeight - 35) * 3 * hRatio + 'px';
	win.timeDate.style.top = 40 * Ratio.vertical + 'px';
	win.timeDate.style.opacity = 1; //smooth coming clock
	win.slideToUnlock.style.opacity = 1; //smooth coming stu

    Clock.init();
	win.timeDate.addEventListener('click', Clock.switchTimeFont);
    
    /*cubeCanvas = doc.getElementById('cube');
	cubeCanvas.width = cubeScale * 80 * hRatio;
	cubeCanvas.height = cubeScale * 88 * hRatio;
	cubeCanvas.style.width = 80 * hRatio + 'px';
	cubeCanvas.style.height = 88 * hRatio + 'px';
	cubeCanvas.style.left = -20 * hRatio + win.innerWidth -
        (setsWidth - 15) * 3 * hRatio -
        16 * hRatio + 'px';
	cubeCanvas.style.top = -20 * hRatio + win.innerHeight -
        (floorHeight + setsHeight - 60) * 3 * hRatio -
        16 * hRatio + 'px';*/
    
    //// OpenGL TEST SECTION ////
    glCanvas = doc.getElementById('glCanvas');
	
    glCanvas.width = cubeScale * 80 * hRatio;
	glCanvas.height = cubeScale * 88 * hRatio;
	glCanvas.style.width = 80 * hRatio + 'px';
	glCanvas.style.height = 88 * hRatio + 'px';
	glCanvas.style.left = -20 * hRatio + win.innerWidth -
        (setsWidth - 15) * 3 * hRatio -
        16 * hRatio + 'px';
	glCanvas.style.top = -20 * hRatio + win.innerHeight -
        (floorHeight + setsHeight - 60) * 3 * hRatio -
        16 * hRatio + 'px';
    
	/*glCanvas.style.width = '100%';
	glCanvas.style.height = '100%';
    glCanvas.width = cubeScale * innerWidth;
	glCanvas.height = cubeScale * innerHeight;*/
    /////////////////////////////
    
    
	x = win.innerWidth -
        (setsWidth - 15) * 3 * hRatio -
        16 * hRatio;
	y = win.innerHeight -
        (floorHeight + setsHeight - 60) * 3 * hRatio -
        16 * hRatio;
    
    Environment.init(doc.getElementById('scenery'));
    Gomez.init(doc.getElementById('gomez'));
    Signal.init(doc.getElementById('signal'));
    Sun.init(doc.getElementById('clouds'));
    Clouds.init(doc.getElementById('clouds'));
    
    //// OpenGL SECTION ////
    webGLRenderer = new Engine3D.WebGLRenderer(glCanvas);
    
    CubeShard.init();
    /*var cube = new Engine3D.Cube(
        -cubeScale * 16 * hRatio,
        -cubeScale * 16 * hRatio,
        -cubeScale * 16 * hRatio,
        cubeScale * 32 * hRatio,
        cubeScale * 32 * hRatio,
        cubeScale * 32 * hRatio
    );
    var cubeX0 = cubeScale *
                (win.innerWidth -
                     (setsWidth - 15) * 3 * hRatio
                ),
        cubeY0 = cubeScale *
                (win.innerHeight -
                     (floorHeight + setsHeight - 60) * 3 * hRatio
                );
    cube.translation = [cubeX0, cubeY0, 0];
    cube.rotationAxis = [1, 1, 1];
    cube.rotationAngle = 0;
    
    cube.textureImage = ImageManager.images['cubeShardFace.png'];
    
    var bigCube = new Engine3D.Cube(
        0,
        0,
        0,
        cubeScale * 160 * hRatio,
        cubeScale * 160 * hRatio,
        cubeScale * 160 * hRatio
    );
    
    bigCube.translation = [cubeScale * 200 * hRatio, cubeScale * 300 * hRatio, 0];
    bigCube.rotationAngles = [
        Engine3D.Math.degToRad(-55),
        Engine3D.Math.degToRad(45),
        0
    ];
    bigCube.textureImage = ImageManager.images['faceExt.png'];
    bigCube.enableAlpha = true;
    
    bigCube.textureOrientation = [
        Engine3D.Math.degToRad(0),
        Engine3D.Math.degToRad(90),
        Engine3D.Math.degToRad(-90),
        Engine3D.Math.degToRad(180),
        Engine3D.Math.degToRad(-90),
        Engine3D.Math.degToRad(180)
    ];
    
    
    var cubeInt = new Engine3D.Cube(
        cubeScale * 8 * hRatio,
        cubeScale * 8 * hRatio,
        cubeScale * 8 * hRatio,
        cubeScale * 144 * hRatio,
        cubeScale * 144 * hRatio,
        cubeScale * 144 * hRatio
    );
    cubeInt.translation = [cubeScale * 200 * hRatio, cubeScale * 300 * hRatio, 0];
    cubeInt.rotationAngles = [
        Engine3D.Math.degToRad(-45),
        Engine3D.Math.degToRad(55),
        0
    ];
    cubeInt.textureImage = ImageManager.images['faceInt.png'];*/
    
    webGLRenderer.addObject(CubeShard);
    //webGLRenderer.addObject(cubeInt);
    //webGLRenderer.addObject(bigCube);
    /////////////////////////////
    
	RenderersManager.init([
        webGLRenderer,
        Environment,
		Gomez,
		Clouds,
		Signal
	]);
}

//function onloadHandler() {
function start() {
	'use strict';
	var sceneryCanvas,
		cloudsCanvas,
        win = window,
        doc = document,
        innerWidth = win.innerWidth,
        innerHeight = win.innerHeight,
        pixel = Config.pixel;
    
    ImageManager.init();
	doc.addEventListener('allImagesLoaded', function () {
		allImagesLoaded(ImageManager);
	});
    
	win.slideToUnlock.style.fontFamily = pixel ? 'pixel' : 'fezzy';
	win.timeDate.style.fontFamily = pixel ? 'pixel' : 'fezzy';

	Ratio.horizontal = innerWidth / 320.0;
	Ratio.vertical = innerHeight / 480.0;

    sceneryCanvas = doc.getElementById('scenery');
	sceneryCanvas.width = innerWidth;
	sceneryCanvas.height = innerHeight;
    cloudsCanvas = doc.getElementById('clouds');
	cloudsCanvas.width = innerWidth;
	cloudsCanvas.height = innerHeight;
}

//by Acemond

var console = console || {},
    Float32Array = Float32Array || {},
    Engine3D = Engine3D || {};

Engine3D.WebGLRenderer = function (canvas) {
    'use strict';
    
    var gl,
        program,
        matrix,
        matrixStack = [],
        scene = [],
        then,
        
        canvasWidth = canvas.width,
        canvasHeight = canvas.height,
        
        translation = [0, 0, 0],
        rotationUAngle = 0,
        rotationUAxis = [0, 0, 0];
        
    
    function initGL(canvas) {
        try {
            gl = canvas.getContext('webgl') ||
                canvas.getContext('experimental-webgl');
            
            gl.enable(gl.CULL_FACE);
            gl.enable(gl.DEPTH_TEST);
            
            //gl.blendEquation(gl.FUNC_ADD);
            //gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        } catch (e) {
            console.err('3DEngine.WebGLRenderer: ' + e);
        }
        if (!gl) {
            console.warn("Couldn't initialize WebGL!");
        }
    }
    function compileShader(shaderSource, shaderType) {
        var shader = gl.createShader(shaderType);

        gl.shaderSource(shader, shaderSource);
        gl.compileShader(shader);

        return shader;
    }
    function initShaders() {
        var vertexShaderSource,
            framgmentShaderSource,
            vertexShader,
            fragmentShader;
        
        vertexShaderSource = document.getElementById('3d-vertex-shader').textContent;
        framgmentShaderSource = document.getElementById('3d-texture-fragment-shader').textContent;
        
        vertexShader = compileShader(vertexShaderSource, gl.VERTEX_SHADER);
        fragmentShader = compileShader(framgmentShaderSource, gl.FRAGMENT_SHADER);
        
        // Create the shader program
        program = gl.createProgram();
        gl.attachShader(program, vertexShader);
        gl.attachShader(program, fragmentShader);
        gl.linkProgram(program);
        gl.useProgram(program);
        
        program.vertexPositionAttribute = gl.getAttribLocation(program, 'a_position');
        gl.enableVertexAttribArray(program.vertexPositionAttribute);
        
        program.textureCoordAttribute = gl.getAttribLocation(program, 'a_texCoord');
        gl.enableVertexAttribArray(program.textureCoordAttribute);
        
        program.matrixLocation = gl.getUniformLocation(program, "u_matrix");
        program.alphaUniform = gl.getUniformLocation(program, "uAlpha");
        gl.uniform1f(program.alphaUniform, 1.0);
    }
    function initPositionBuffer(object) {
        var len,
            i,
            node,
            objectNodes = object.nodes,
            objectVertices = object.vertices,
            positionVertices = [],
            vertexCount = 0;
        
        len = objectVertices.length;
        for (i = 0; i < len; i += 1) {
            node = objectVertices[i] * 3;
            
            positionVertices.push(objectNodes[node]);
            positionVertices.push(objectNodes[node + 1]);
            positionVertices.push(objectNodes[node + 2]);
            
            vertexCount += 1;
        }
        
        object.vertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, object.vertexPositionBuffer);
        gl.bufferData(
            gl.ARRAY_BUFFER,
            new Float32Array(positionVertices),
            gl.STATIC_DRAW
        );
        object.vertexPositionBuffer.itemSize = 3;
        object.vertexPositionBuffer.numItems = vertexCount;
    }
    function initTexture(object) {
        object.texture = gl.createTexture();
        
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.bindTexture(gl.TEXTURE_2D, object.texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, object.textureImage);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

        gl.bindTexture(gl.TEXTURE_2D, null);
    }
    function initTextureBuffer(object) {
        var len,
            i,
            j,
            textureVertices = [],
            vertexCount = 0;
        
        len = object.vertices.length;
        for (i = 0; i < len; i += 1) {
            textureVertices = textureVertices.concat(
                Engine3D.Math.apply2DRotation([
                    0, 0,
                    0, 1,
                    1, 0,
                    0, 1,
                    1, 1,
                    1, 0
                ], object.textureOrientation[i]));
            vertexCount += 1;
        }
        
        object.vertexTextureBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, object.vertexTextureBuffer);
        
        gl.bufferData(
            gl.ARRAY_BUFFER,
            new Float32Array(textureVertices),
            gl.STATIC_DRAW
        );
        object.vertexTextureBuffer.itemSize = 2;
        object.vertexTextureBuffer.numItems = vertexCount;
    }
    
    function mvPushMatrix() {
        var copy = Engine3D.Math.createMatrix();
        Engine3D.Math.copyMatrix(matrix, copy);
        matrixStack.push(copy);
    }
    function mvPopMatrix() {
        if (matrixStack.length === 0) {
            throw "Invalid popMatrix!";
        }
        matrix = matrixStack.pop();
    }
    
    this.render = function () {
        var len,
            i,
            object;
        
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        if (scene.length === 0) {
            return;
        }
        
        matrix = Engine3D.Math.createMatrix();
        //Engine3D.Math.translateMatrix(matrix, translation);
        //Engine3D.Math.rotateUMatrix(matrix, Engine3D.Math.degToRad(rotationUAngle), rotationUAxis);
        
        len = scene.length;
        for (i = 0; i < len; i += 1) {
            object = scene[i];
            
            mvPushMatrix();
            Engine3D.Math.translateMatrix(matrix, object.translation);
            /*Engine3D.Math.rotateUMatrix(
                matrix,
                object.rotationAngles[0],
                [1, 0, 0]
            );
            Engine3D.Math.rotateUMatrix(
                matrix,
                object.rotationAngles[1],
                [0, 1, 0]
            );
            Engine3D.Math.rotateUMatrix(
                matrix,
                object.rotationAngles[2],
                [0, 0, 1]
            );*/
            Engine3D.Math.rotateUMatrix(
                matrix,
                object.rotationAngle,
                object.rotationAxis
            );
            
            Engine3D.Math.projectMatrix(
                matrix,
                canvasWidth,
                canvasHeight,
                canvasWidth * 2.0
            );
            
            gl.activeTexture(gl.TEXTURE0);  // Necessary?
            gl.bindTexture(gl.TEXTURE_2D, object.texture);
            gl.uniform1i(program.samplerUniform, 0);  // Necessary?
            
            gl.bindBuffer(gl.ARRAY_BUFFER, object.vertexPositionBuffer);
            gl.vertexAttribPointer(
                program.vertexPositionAttribute,
                object.vertexPositionBuffer.itemSize,
                gl.FLOAT, false, 0, 0
            );
            
            gl.bindBuffer(gl.ARRAY_BUFFER, object.vertexTextureBuffer);
            gl.vertexAttribPointer(
                program.textureCoordAttribute,
                object.vertexTextureBuffer.itemSize,
                gl.FLOAT, false, 0, 0
            );
            
            gl.uniformMatrix4fv(program.matrixLocation, false, matrix);
            gl.drawArrays(
                gl.TRIANGLES,
                0,
                object.vertexPositionBuffer.numItems
            );
            mvPopMatrix();
        }
    }
    this.update = function (dt) {
        var len,
            i;
        
        len = scene.length
        for (i = 0; i < len; i += 1) {
            scene[i].update(dt);
            //scene[i].rotationAngle += 0.01;
        }
    }
    
    // WebGL Start
    initGL(canvas);
    initShaders();
    
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    
    //requestAnimationFrame(tick);
    
    
    
    // Methods accessible externally
    this.addObject = function (object) {
        initTexture(object);
        
        initPositionBuffer(object);
        initTextureBuffer(object);
        
        scene.push(object);
    };
};





















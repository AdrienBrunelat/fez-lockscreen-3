var Engine3D = { REVISION: '1' };

Engine3D.normVect = function (u) {
    'use strict';
    var math = Math,
        pow = math.pow,
        q = math.sqrt(pow(u[0], 2) + pow(u[1], 2) + pow(u[2], 2));

    return [u[0] / q, u[1] / q, u[2] / q];
};
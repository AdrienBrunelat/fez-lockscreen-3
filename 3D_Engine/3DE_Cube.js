var Engine3D = Engine3D || {};

Engine3D.Cube = function (x0, y0, z0, w, h, d) {
    'use strict';
    var x1 = x0,
        x2 = x0 + w,
        y1 = y0,
        y2 = y0 + h,
        z1 = z0,
        z2 = z0 + d;
    
    Engine3D.Object3D.call(this, [x0 + w / 2, y0 + h / 2, z0 + d / 2]);
	this.nodes = [
        x1, y1, z1,  // 0
        x2, y1, z1,  // 1
        x1, y2, z1,  // 2
        x1, y1, z2,  // 3
        x2, y2, z1,  // 4
        x1, y2, z2,  // 5
        x2, y1, z2,  // 6
        x2, y2, z2   // 7
    ];
    
    this.vertices = [
        0, 2, 1,
        2, 4, 1,

        1, 4, 6,
        4, 7, 6,

        6, 7, 3,
        7, 5, 3,

        3, 5, 0,
        5, 2, 0,

        2, 5, 4,
        5, 7, 4,

        3, 0, 6,
        0, 1, 6
    ];
    
    this.textureOrientation = [0, 0, 0, 0, 0, 0];
        
    this.textureImage = undefined;
};

Engine3D.Cube.prototype = {
    constructor: Engine3D.Cube
};
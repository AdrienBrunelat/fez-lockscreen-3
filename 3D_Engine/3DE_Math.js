var Engine3D = Engine3D || {};

Engine3D.Math = {
    createMatrix: function () {
        'use strict';
        
        return [
            1, 0,  0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        ];
    },
    identityMatrix: function (a) {
        'use strict';
        
        a = this.createMatrix();
        return a;
    },
    translationMatrix: function (tx, ty, tz) {
        'use strict';
        
        return [
            1,  0,  0,  0,
            0,  1,  0,  0,
            0,  0,  1,  0,
            tx, ty, tz, 1
        ];
    },
    translateMatrix: function (a, v) {
        'use strict';
        var x = v[0],
            y = v[1],
            z = v[2];
        
        a[12] = a[0] * x + a[4] * y + a[8] * z + a[12];
        a[13] = a[1] * x + a[5] * y + a[9] * z + a[13];
        a[14] = a[2] * x + a[6] * y + a[10] * z + a[14];
        a[15] = a[3] * x + a[7] * y + a[11] * z + a[15];
        
        return a;
    },
    rotation2D: function (theta) {
        'use strict';
        var c = Math.cos(theta),
            s = Math.sin(theta);
        
        return [
            c, s,
            -s, c
        ];
    },
    rotationXMatrix: function (theta) {
        'use strict';
        var c = Math.cos(theta),
            s = Math.sin(theta);
        
        return [
            1,  0,  0,  0,
            0,  c,  s,  0,
            0, -s,  c,  0,
            0,  0,  0,  1
        ];
            
    },
    rotationYMatrix: function (theta) {
        'use strict';
        var c = Math.cos(theta),
            s = Math.sin(theta);
        
        return [
            c,  0, -s,  0,
            0,  1,  0,  0,
            s,  0,  c,  0,
            0,  0,  0,  1
        ];
            
    },
    rotationZMatrix: function (theta) {
        'use strict';
        var c = Math.cos(theta),
            s = Math.sin(theta);
        
        return [
            c,  s,  0,  0,
            -s, c,  0,  0,
            0,  0,  1,  0,
            0,  0,  0,  1
        ];
            
    },
    rotationUMatrix: function (ux, uy, uz, theta) {
        'use strict';
        var math = Math,
            c = math.cos(theta),
            s = math.sin(theta),
            pow = math.pow,
            u;
        
		u = this.normVector(ux, uy, uz);
        
        ux = u[0];
        uy = u[1];
        uz = u[2];
        
        return [
            c + pow(ux, 2) * (1 - c),
            uy * ux * (1 - c) + uz * s,
            uz * ux * (1 - c) - uy * s,
            0,
            ux * uy * (1 - c) - uz * s,
            c + pow(uy, 2) * (1 - c),
            uz * uy * (1 - c) + ux * s,
            0,
            ux * uz * (1 - c) + uy * s,
            uy * uz * (1 - c) - ux * s,
            c + pow(uz, 2) * (1 - c),
            0,
            0, 0, 0, 1
        ];
    },
    rotateUMatrix: function (out, rad, axis) {
        'use strict';
        var x = axis[0], y = axis[1], z = axis[2],
            len = Math.sqrt(x * x + y * y + z * z),
            s, c, t,
            a00, a01, a02, a03,
            a10, a11, a12, a13,
            a20, a21, a22, a23,
            b00, b01, b02,
            b10, b11, b12,
            b20, b21, b22;

        if (!len) {
            return null;
        }

        len = 1 / len;
        x *= len;
        y *= len;
        z *= len;

        s = Math.sin(rad);
        c = Math.cos(rad);
        t = 1 - c;

        a00 = out[0];
        a01 = out[1];
        a02 = out[2];
        a03 = out[3];
        a10 = out[4];
        a11 = out[5];
        a12 = out[6];
        a13 = out[7];
        a20 = out[8];
        a21 = out[9];
        a22 = out[10];
        a23 = out[11];

        b00 = x * x * t + c;
        b01 = y * x * t + z * s;
        b02 = z * x * t - y * s;
        
        b10 = x * y * t - z * s;
        b11 = y * y * t + c;
        b12 = z * y * t + x * s;
        
        b20 = x * z * t + y * s;
        b21 = y * z * t - x * s;
        b22 = z * z * t + c;

        out[0] = a00 * b00 + a10 * b01 + a20 * b02;
        out[1] = a01 * b00 + a11 * b01 + a21 * b02;
        out[2] = a02 * b00 + a12 * b01 + a22 * b02;
        out[3] = a03 * b00 + a13 * b01 + a23 * b02;
        out[4] = a00 * b10 + a10 * b11 + a20 * b12;
        out[5] = a01 * b10 + a11 * b11 + a21 * b12;
        out[6] = a02 * b10 + a12 * b11 + a22 * b12;
        out[7] = a03 * b10 + a13 * b11 + a23 * b12;
        out[8] = a00 * b20 + a10 * b21 + a20 * b22;
        out[9] = a01 * b20 + a11 * b21 + a21 * b22;
        out[10] = a02 * b20 + a12 * b21 + a22 * b22;
        out[11] = a03 * b20 + a13 * b21 + a23 * b22;

        return out;
    },
    normVector: function (ux, uy, uz) {
        'use strict';
        var math = Math,
            pow = math.pow,
            q = math.sqrt(pow(ux, 2) + pow(uy, 2) + pow(uz, 2));

        return [ux / q, uy / q, uz / q];
    },
    scalingMatrix: function (sx, sy, sz) {
        'use strict';
        
        return [
            sx, 0,  0, 0,
            0,  sy, 0, 0,
            0,  0, sz, 0,
            0,  0,  0, 1
        ];
    },
    projectionMatrix: function (w, h, d) {
        'use strict';
        
        return [
            2 / w, 0,      0,     0,
            0,     -2 / h, 0,     0,
            0,     0,      2 / d, 0,
            -1,    1,      0,     1
        ];
    },
    projectMatrix: function (out, w, h, d) {
        'use strict';
        var a00 = out[0],
            a01 = out[1],
            a02 = out[2],
            a03 = out[3],
            a10 = out[4],
            a11 = out[5],
            a12 = out[6],
            a13 = out[7],
            a20 = out[8],
            a21 = out[9],
            a22 = out[10],
            a23 = out[11],
            a30 = out[12],
            a31 = out[13],
            a32 = out[14],
            a33 = out[15],
            b00 = 2.0 / w,
            b11 = -2.0 / h,
            b22 = 2.0 / d;
        
        out[0] = a00 * b00 - a03;
        out[1] = a01 * b11 + a03;
        out[2] = a02 * b22;
      //out[3] = a03
        out[4] = a10 * b00 - a13;
        out[5] = a11 * b11 + a13;
        out[6] = a12 * b22;
      //out[7] = a13
        out[8] = a20 * b00 - a23;
        out[9] = a21 * b11 + a23;
        out[10] = a22 * b22;
      //out[11] = a23
        out[12] = a30 * b00 - a33;
        out[13] = a31 * b11 + a33;
        out[14] = a32 * b22;
      //out[15] = a33
        
        return out;
    },
    apply2DRotation: function (vec2d, theta) {
        'use strict';
        var c = Math.cos(theta),
            s = Math.sin(theta),
            len,
            i,
            x,
            y;
        
        len = vec2d.length;
        for (i = 0; i < len; i += 2) {
            x = vec2d[i] - 0.5;
            y = vec2d[i + 1] - 0.5;
            
            vec2d[i] = (x * c + y * s) + 0.5;
            vec2d[i + 1] = (x * -s + y * c) + 0.5;
        }
        
        return vec2d;
    },
    multiplyMatrices: function multiplyMatrices(a, b) {
        'use strict';
        var a00 = a[0],
            a01 = a[1],
            a02 = a[2],
            a03 = a[3],
            a10 = a[4],
            a11 = a[5],
            a12 = a[6],
            a13 = a[7],
            a20 = a[8],
            a21 = a[9],
            a22 = a[10],
            a23 = a[11],
            a30 = a[12],
            a31 = a[13],
            a32 = a[14],
            a33 = a[15],
            b00 = b[0],
            b01 = b[1],
            b02 = b[2],
            b03 = b[3],
            b10 = b[4],
            b11 = b[5],
            b12 = b[6],
            b13 = b[7],
            b20 = b[8],
            b21 = b[9],
            b22 = b[10],
            b23 = b[11],
            b30 = b[12],
            b31 = b[13],
            b32 = b[14],
            b33 = b[15];
            
        return [
            a00 * b00 + a01 * b10 + a02 * b20 + a03 * b30,
            a00 * b01 + a01 * b11 + a02 * b21 + a03 * b31,
            a00 * b02 + a01 * b12 + a02 * b22 + a03 * b32,
            a00 * b03 + a01 * b13 + a02 * b23 + a03 * b33,
            
            a10 * b00 + a11 * b10 + a12 * b20 + a13 * b30,
            a10 * b01 + a11 * b11 + a12 * b21 + a13 * b31,
            a10 * b02 + a11 * b12 + a12 * b22 + a13 * b32,
            a10 * b03 + a11 * b13 + a12 * b23 + a13 * b33,
            
            a20 * b00 + a21 * b10 + a22 * b20 + a23 * b30,
            a20 * b01 + a21 * b11 + a22 * b21 + a23 * b31,
            a20 * b02 + a21 * b12 + a22 * b22 + a23 * b32,
            a20 * b03 + a21 * b13 + a22 * b23 + a23 * b33,
            
            a30 * b00 + a31 * b10 + a32 * b20 + a33 * b30,
            a30 * b01 + a31 * b11 + a32 * b21 + a33 * b31,
            a30 * b02 + a31 * b12 + a32 * b22 + a33 * b32,
            a30 * b03 + a31 * b13 + a32 * b23 + a33 * b33
        ];
    },
    copyMatrix: function (a, out) {
        'use strict';
    
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[3];
        out[4] = a[4];
        out[5] = a[5];
        out[6] = a[6];
        out[7] = a[7];
        out[8] = a[8];
        out[9] = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    },
    radToDeg: function radToDeg(r) {
        'use strict';
        
        return r * 180.0 / Math.PI;
    },
    degToRad: function degToRad(d) {
        'use strict';
        
        return d * Math.PI / 180.0;
    }
};
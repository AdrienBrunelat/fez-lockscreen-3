var Engine3D = Engine3D || {};

Engine3D.Object3D = function (origin) {
    'use strict';
    
    this.origin = origin;
    this.nodes = undefined;
    this.vertices = undefined;
    
    this.texture = undefined;
    
    this.rotationAngle = 0;
    this.rotationAxis = [1, 1, 1];
    this.rotationAngles = [0, 0, 0];
    this.translation = [0, 0, 0];
    
    this.enableAlpha = false;
};

Engine3D.Object3D.prototype = {
    constructor: Engine3D.Object3D,

    addNode: function (x, y, z) {
        'use strict';
        var push = this.nodes.push;
        
        push(x);
        push(y);
        push(z);
    },
    
    addVertex: function (n1, n2, n3) {
        'use strict';
        var push = this.vertices.push;
        
        push(n1);
        push(n2);
        push(n3);
    },
    
    translate: function (tx, ty, tz) {
        'use strict';
        var origin = this.origin,
            nodes = this.nodes,
            len,
            i;
        
        origin[0] += tx;
        origin[1] += ty;
        origin[2] += tz;
        
        len = nodes.length;
        for (i = 0; i < len; i += 3) {
            nodes[i] += tx;
            nodes[i + 1] += tx;
            nodes[i + 2] += tx;
        }
    },
    
    translateX: function (tx) {
        'use strict';
        var nodes = this.nodes,
            len,
            i;
        
        this.origin[0] += tx;
        
        len = nodes.length;
        for (i = 0; i < len; i += 3) {
            nodes[i] += tx;
        }
    },

    translateY: function (ty) {
        'use strict';
        var nodes = this.nodes,
            len,
            i;
        
        this.origin[0] += ty;
        
        len = nodes.length;
        for (i = 1; i < len; i += 3) {
            nodes[i] += ty;
        }
    },

    translateZ: function (tz) {
        'use strict';
        var nodes = this.nodes,
            len,
            i;
        
        this.origin[0] += tz;
        
        len = nodes.length;
        for (i = 2; i < len; i += 3) {
            nodes[i] += tz;
        }
    }

	/*proj_2D: function (face) {
        'use strict';
		var nodes = this.nodes,
            n1 = nodes[face[0]],
			n2 = nodes[face[1]],
			n3 = nodes[face[2]],
            
            n1_x = n1[0],
            n1_y = n1[1],
            n1_z = n1[2],

			v1_x = n1_x - n2[0],
			v1_y = n1_y - n2[1],
			v1_z = n1_z - n2[2],
            
			v2_x = n1_x - n3[0],
			v2_y = n1_y - n3[1],
			v2_z = n1_z - n3[2];

		return [v1_y * v2_z - v1_z * v2_y,
			    v1_z * v2_x - v1_x * v2_z,
			    v1_x * v2_y - v1_y * v2_x];
	}*/
};
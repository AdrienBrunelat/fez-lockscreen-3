/* Global var calls */
var Ratio = Ratio || {},
    Environment = Environment || {},
    ImageManager = ImageManager || {},
    Config = Config || {},
    Sun = Sun || {};

// TODO: Check if clouds are renderer in white and if necessary to render in color
// If so, play with contexts rendering operation (fill the destination in cloud color and then
// operation dest-in

var Clouds = {
    cloudHeight: [60, 130, 170],
    cloudBumpsYPos: [0, 60, 130],
    cloudBumpsHeight: [46, 24, 16],
    cloudBumpsYPosOnCache: [0, 46, 70],
    cloudOpacities: [1, 0.375, 0.175],

    window: window,
    windowHRatio: undefined,
    windowVRatio: undefined,
    windowInnerWidth: undefined,
    windowInnerHeight: undefined,

    canvas: undefined,
    canvasWidth: undefined,
    canvasHeight: undefined,
    canvas2DContext: undefined,

    cloudBumpsCanvas: undefined,
    cloudBumps2DContext: undefined,

    moving: Config.movingClouds,
    cloudAnimPos: [0, 0, 0],
    frame: 0,
    framePeriod: Math.floor(20 / Config.cloudsSpeed),
    dt: 0,
    setsImage: undefined,
    floorHeight: undefined,

    rays: [],

    outdated: true,

    environment: undefined,

    init: function (canvas) {
        'use strict';
        var cloudsSpace = 160,
            wCloud0,
            wCloud1,
            wHole0,
            wHole1,
            holesSpace,

            totalBumpsHeight = this.cloudBumpsHeight[0] +
                this.cloudBumpsHeight[1] +
                this.cloudBumpsHeight[2],

            windowInnerWidth,
            windowInnerHeight,

            floor = Math.floor,
            random = Math.random,
            hRatio = Ratio.horizontal,
            vRatio = Ratio.vertical,
            n;

        this.environment = Environment;

        this.setsImage = ImageManager.images['sets.png'];
        this.floorHeight = ImageManager.images['floor.png'].height;
        windowInnerWidth = this.windowInnerWidth = this.window.innerWidth;
        windowInnerHeight = this.windowInnerHeight = this.window.innerHeight;
        this.windowHRatio = Ratio.horizontal;
        this.windowVRatio = Ratio.vertical;

        this.canvas = canvas;
        this.canvasWidth = canvas.width;
        this.canvasHeight = canvas.height;
        this.canvas2DContext = canvas.getContext('2d');
        this.canvas2DContext.webkitImageSmoothingEnabled = false;

        this.cloudBumpsCanvas = document.createElement("canvas");
        this.cloudBumpsCanvas.width = 2 * windowInnerWidth;
        this.cloudBumpsCanvas.height = totalBumpsHeight * vRatio;
        this.cloudBumps2DContext = this.cloudBumpsCanvas.getContext('2d');

        for (n = 0; n < 3; n += 1) {
            //for (n = 3; n--;) {
            wCloud0 = floor((cloudsSpace - 142) * random()) + 42;
            wCloud1 = floor((cloudsSpace - wCloud0 - 100) * random()) + 34;

            holesSpace = 160 - (wCloud0 + wCloud1);
            wHole0 = floor((holesSpace - 66) * random()) + 22;
            wHole1 = floor((holesSpace - (wHole0 + 44)) * random()) + 22;

            wCloud0 *= 2 * hRatio;
            wCloud1 *= 2 * hRatio;

            wHole0 *= 2 * hRatio;
            wHole1 *= 2 * hRatio;

            this.drawCloudBumps(
                wHole0,
                this.cloudBumpsYPosOnCache[n] * vRatio,
                wCloud0,
                this.cloudBumpsHeight[n] * vRatio,
                3 - n
            );
            this.drawCloudBumps(
                wHole0 + wCloud0 + wHole1,
                this.cloudBumpsYPosOnCache[n] * vRatio,
                wCloud1,
                this.cloudBumpsHeight[n] * vRatio,
                3 - n
            );
            this.cloudBumps2DContext.drawImage(
                this.cloudBumpsCanvas,
                windowInnerWidth,
                0
            );

            this.cloudAnimPos[n] = floor(windowInnerWidth * random());
        }
    },
    drawCloudBumps: function (x, y, w, h, hBump, noBumpIn, noBumpOut) {
        'use strict';
        var i,
            ctx = this.cloudBumps2DContext;

        if (noBumpIn) {
            ctx.fillRect(x, y, 6 * hBump, h);
        } else {
            for (i = 0; i < hBump; i += 1) {
                //for (i = hBump; i--;) {
                ctx.fillRect(x + 3 * i, y, 3, 3 * (i + 1));
                ctx.fillRect(x + 3 * (i + hBump), y, 3, h - 3 * (hBump - i));
            }
        }
        ctx.fillRect(x + 6 * hBump, y, w - 12 * hBump, h);

        if (noBumpOut) {
            ctx.fillRect(x + w - 6 * hBump, y, 6 * hBump, h);
        } else {
            for (i = 0; i < hBump; i += 1) {
                //for (i = hBump; i--;) {
                ctx.fillRect(x + w - 6 * hBump + 3 * i, y, 3, h - 3 * (i + 1));
                ctx.fillRect(x + w - 6 * hBump + 3 * (i + hBump), y, 3, 3 * (hBump - i));
            }
        }
    },
    update: function (dt) {
        'use strict';
        var rays = this.rays,
            len,
            i,
            n,
            environment = this.environment,
            raysColor = environment.raysColor;

        len = rays.length;
        for (i = 0; i < len; i += 1) {
            rays[i].update(dt);
        }
        for (i = len - 1; i > 0; i -= 1) {
            if (rays[i].over) {
                rays.splice(i, 1);
            }
        }

        //TODO: Must find a solution for n > 1
        n = Math.floor((this.dt + dt) / (1000.0 / 60.0));

        if (n === 0) {
            this.dt += dt;
        } else {
            n = 1;
            this.dt = 0;
            this.frame += n;

            if (this.moving) {
                //for (i = 3; i--;) {
                for (i = 0; i < 3; i += 1) {
                    if ((this.frame + this.framePeriod) %
                        (this.framePeriod * (i + 1)) === 0) {
                        this.cloudAnimPos[i] += 1;
                        this.outdated = true;
                    }
                    if (this.cloudAnimPos[i] >= this.cloudBumpsCanvas.width / 2) {
                        this.cloudAnimPos[i] = 0;
                        this.outdated = true;
                    }
                }
                if (this.frame >= 3 * this.framePeriod) {
                    this.frame = 0;
                }
            }
        }

        if (environment.raysFrequency === 0) {
            this.rays = [];
        } else if (raysColor !== environment.RaysEnum.None) {
            if (Math.floor((Math.random() * environment.raysFrequency) + 1) % environment.raysFrequency === 0) {
                rays.push(Sun.castNewRay(raysColor));
            }
        }

    },
    render: function () {
        'use strict';
        var ctx = this.canvas2DContext,
            cloudBumpsCanvas = this.cloudBumpsCanvas,
            cloudHeight = this.cloudHeight,
            vRatio = this.windowVRatio,
            environment = this.environment,
            cloudOpacities = this.cloudOpacities,
            i,
            j,
            len = this.rays.length,
            raysLen,
            ray;


        if (!this.outdated && len === 0) {
            return;
        }

        this.cloudBumps2DContext.globalCompositeOperation = 'source-atop';
        this.cloudBumps2DContext.fillStyle = environment.cloudsColor;
        this.cloudBumps2DContext.fillRect(
            0,
            0,
            cloudBumpsCanvas.width,
            cloudBumpsCanvas.height
        );
        this.cloudBumps2DContext.globalCompositeOperation = 'source-over';

        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);

        for (i = 3; i--;) {
            raysLen = this.rays.length;
            for (j = 0; j < raysLen; j += 1) {
                ray = this.rays[j];
                if (ray.z === (2 - i)) {
                    ray.render();
                }
            }

            ctx.globalAlpha = cloudOpacities[i];
            ctx.fillStyle = environment.cloudsColor;
            ctx.fillRect(
                0,
                0,
                this.windowInnerWidth,
                this.cloudHeight[i] * vRatio
            );
            ctx.drawImage(
                cloudBumpsCanvas,
                this.cloudAnimPos[i],
                this.cloudBumpsYPosOnCache[i] * vRatio,
                this.windowInnerWidth,
                this.cloudBumpsHeight[i] * vRatio,

                0,
                cloudHeight[i] * vRatio,
                this.windowInnerWidth,
                this.cloudBumpsHeight[i] * vRatio
            );
            ctx.globalAlpha = 1;
        }

        for (j = 0; j < raysLen; j += 1) {
            ray = this.rays[j];
            if (ray.z === 3) {
                ray.render();
            }
        }

        this.renderSetsMask(ctx);
        this.outdated = false;
    },
    renderSetsMask: function (ctx) {
        'use strict';
        var hRatio = this.windowHRatio,
            setsImage = this.setsImage,
            setsWidth = setsImage.width,
            setsHeight = setsImage.height,
            innerWidth = this.windowInnerWidth,
            innerHeight = this.windowInnerHeight;

        ctx.globalCompositeOperation = 'destination-out';
        ctx.drawImage(
            setsImage,
            0,
            0,
            setsWidth,
            setsHeight,
            innerWidth - (setsWidth * 3 * hRatio),
            innerHeight - ((this.floorHeight + setsHeight - 20) * 3 * hRatio),
            setsWidth * 3 * hRatio,
            setsHeight * 3 * hRatio
        );
        ctx.globalCompositeOperation = 'source-over';
    }
};




(function () {
    'use strict';
    var lastFrame,
        method,
        now,
        queue,
        requestAnimationFrame,
        timer,
        len,
        ref1;

    method = 'native';
    now = Date.now || function () {
            return new Date().getTime();
        };
    requestAnimationFrame = window.requestAnimationFrame;

    if (requestAnimationFrame === null) {
        method = 'timer';
        lastFrame = 0;
        queue = timer = null;
        requestAnimationFrame = function (callback) {
            var fire,
                nextFrame,
                time;

            if (queue !== null) {
                queue.push(callback);
                return;
            }

            time = now();
            nextFrame = Math.max(0, 16.66 - (time - lastFrame));
            queue = [callback];
            lastFrame = time + nextFrame;
            fire = function () {
                var cb,
                    q,
                    j,
                    len1;

                q = queue;
                queue = null;
                for (j = 0, len1 = q.length; j < len1; j += 1) {
                    cb = q[j];
                    cb(lastFrame);
                }
            };
            timer = setTimeout(fire, nextFrame);
        };
    }

    requestAnimationFrame(function (time) {
        var offset,
            ref1;

        if (time < 1e12) {
            if (((ref1 = window.performance) !== null ? ref1.now : void 0) !== null) {
                requestAnimationFrame.now = function () {
                    return window.performance.now();
                };
                requestAnimationFrame.method = 'native-highres';
            } else {
                offset = now() - time;
                requestAnimationFrame.now = function () {
                    return now() - offset;
                };
                requestAnimationFrame.method = 'native-highres-noperf';
            }
        } else {
            requestAnimationFrame.now = now;
        }
    });

    requestAnimationFrame.now =
        ((ref1 = window.performance) !== null ? ref1.now : void 0) !== null ?
            function () {
                return window.performance.now();
            } :
            now;

    requestAnimationFrame.method = method;
    window.requestAnimationFrame = requestAnimationFrame;
}());
/* Global var calls */
var Ratio = Ratio || {},
    ImageManager = ImageManager || {};

var Environment = {
    StateEnum: {
        AM5To6: 0,
        AM6To7: 1,
        AM7To8: 2,
        AM8To9: 3,
        midday: 4,
        PM5To6: 5,
        PM6To7: 6,
        PM7To8: 7,
        night: 8
    },
    raysColor: undefined,
    raysFrequency: undefined,
    RaysEnum: {
        Yellow: [255, 255, 127],
        White: [255, 255, 255],
        YellowToWhite: 1,
        WhiteToYellow: 2,
        None: undefined
    },
    clock: undefined,
    
    canvas: undefined,
    canvas2DContext: undefined,
    window: window,
    windowInnerWidth: window.innerWidth,
    windowInnerHeight: window.innerHeight,
    windowHRatio: undefined,
    windowVRatio: undefined,
    
    floorImage: undefined,
    setsImage: undefined,
    nightFloorImage: undefined,
    nightSetsImage: undefined,
    
	state: undefined,
	bgImg: undefined,
	bgPosY: undefined,
	bgMImg: undefined,
	is_bgNight: undefined,
    mask: undefined,
	floorMaskOpacity: undefined,
    cloudsColorRGB: [undefined, undefined, undefined],
    cloudsMColorRGB: [undefined, undefined, undefined],
    cloudsColor: undefined,
	starsPos: Math.floor(320.0 * Math.random()),
    
    init: function (canvas) {
        'use strict';
        var images = ImageManager.images;
        
        this.canvas = canvas;
        this.canvas2DContext = canvas.getContext('2d');
        
        this.windowHRatio = Ratio.horizontal;
        this.windowVRatio = Ratio.vertical;
        
        this.floorImage = images["floor.png"];
        this.setsImage = images["sets.png"];
        this.nightFloorImage = images['floor_night.png'];
        this.nightSetsImage = images['sets_night.png'];
    },
    update: function (dt) {
		'use strict';
		var hours = this.clock.currentTime.getHours(),
			minutes = this.clock.currentTime.getMinutes(),
            r,
            g,
            b;

		this.floorMaskOpacity = 0;
	
		if (hours > 8 && hours < 17) {
			if (this.state !== this.StateEnum.midday) {
				this.state = this.StateEnum.midday;
                this.raysColor = [255, 255, 255];
                this.raysFrequency = 100;
				this.bgImg = ImageManager.images['BG_midday.png'];
				this.bgPosY = 0;
				this.cloudsColor = "#a7fcff";
				this.is_bgNight = false;
				this.mask = false;
	
				this.outdated = true;
			}
		} else if ((hours > 19 || hours < 5)) {
			if (this.state !== this.StateEnum.night) {
				this.state = this.StateEnum.night;
                this.raysColor = null;
                this.raysFrequency = 0;
				this.bgImg = ImageManager.images['BG_night.png'];
				this.bgPosY = 0;
				this.cloudsColor = "#353454";
				this.is_bgNight = true;
				this.mask = false;
	
				this.outdated = true;
			}
		} else {
            this.mask = true;
            switch (hours) {
            case 5:
                if (this.state !== this.StateEnum.AM5To6) {
                    this.state = this.StateEnum.AM5To6;
                    this.raysColor = [255, 255, 127];
                    this.raysFrequency = 200;  // this is the original value, it is raised later in the code
                    this.bgImg = ImageManager.images['BG_night.png'];
                    this.bgPosY = 0;
				    this.cloudsColorRGB = [53, 52, 84]; //"#353454";
                    this.bgMImg = ImageManager.images['BG_sunset_sunrise.png'];
                    this.bgMPosY = 490;
				    this.cloudsMColorRGB = [241, 160, 193]; //"#f1a0c1";
                    this.is_bgNight = true;
                }
                this.floorMaskOpacity = minutes / 180;
                this.outdated = true;
                break;

            case 6:
                if (this.state !== this.StateEnum.AM6To7) {
                    this.state = this.StateEnum.AM6To7;
                    this.raysColor = [255, 255, 127];
                    this.raysFrequency = 100;
                    this.bgImg = ImageManager.images['BG_sunset_sunrise.png'];
                    this.bgPosY = 490;
				    this.cloudsColorRGB = [241, 160, 193]; //"#f1a0c1";
                    this.bgMImg = ImageManager.images['BG_morning_evening.png'];
                    this.bgMPosY = 0;
				    this.cloudsMColorRGB = [255, 198, 181]; //"#ffc6b5";
                    this.is_bgNight = true;
                }
                this.floorMaskOpacity = (minutes + 60) / 180;
                this.outdated = true;
                break;

            case 7:
                this.floorMaskOpacity = (minutes + 120) / 180;
                if (this.state !== this.StateEnum.AM7To8) {
                    this.state = this.StateEnum.AM7To8;
                    this.raysColor = [255, 255, 127];
                    this.raysFrequency = 100;
                    this.bgImg = ImageManager.images['BG_morning_evening.png'];
                    this.bgPosY = 0;
				    this.cloudsColorRGB = [255, 198, 181]; //"#ffc6b5";
                    this.bgMImg = ImageManager.images['BG_morning_end.png'];
                    this.bgMPosY = 0;
				    this.cloudsMColorRGB = [248, 234, 171]; //"#f8eaab";
                    this.is_bgNight = true;
                }
                this.floorMaskOpacity = (minutes + 120) / 180;
                this.outdated = true;
                break;

            case 8:
                if (this.state !== this.StateEnum.AM8To9) {
                    this.state = this.StateEnum.AM8To9;
                    this.raysColor = this.RaysEnum.YellowToWhite;
                    this.raysFrequency = 100;
                    this.bgImg = ImageManager.images['BG_morning_end.png'];
                    this.bgPosY = 0;
				    this.cloudsColorRGB = [248, 234, 171]; //"#f8eaab";
                    this.bgMImg = ImageManager.images['BG_midday.png'];
                    this.bgMPosY = 0;
				    this.cloudsMColorRGB = [167, 252, 255]; //"#a7fcff";
                    this.is_bgNight = true;
                }
                this.floorMaskOpacity = 1;
                this.outdated = true;
                break;

            case 17:
                if (this.state !== this.StateEnum.PM5To6) {
                    this.state = this.StateEnum.PM5To6;
                    this.raysColor = this.RaysEnum.WhiteToYellow;
                    this.raysFrequency = 100;
                    this.bgImg = ImageManager.images['BG_midday.png'];
                    this.bgPosY = 0;
				    this.cloudsColorRGB = [167, 252, 255]; //"#a7fcff";
                    this.bgMImg = ImageManager.images['BG_morning_evening.png'];
                    this.bgMPosY = 480;
				    this.cloudsMColorRGB = [236, 171, 169]; //"#ecaba9";
                    this.is_bgNight = false;
                }
                this.floorMaskOpacity = minutes / 180;
                this.outdated = true;
                break;

            case 18:
                if (this.state !== this.StateEnum.PM6To7) {
                    this.state = this.StateEnum.PM6To7;
                    this.raysColor = [255, 255, 127];
                    this.raysFrequency = 100;  // Base frequency, lowers with time
                    this.bgImg = ImageManager.images['BG_morning_evening.png'];
                    this.bgPosY = 480;
				    this.cloudsColorRGB = [236, 171, 169]; //"#ecaba9";
                    this.bgMImg = ImageManager.images['BG_sunset_sunrise.png'];
                    this.bgMPosY = 0;
				    this.cloudsMColorRGB = [142, 73, 120]; //"#8e4978";
                    this.is_bgNight = false;
                }
                this.floorMaskOpacity = (minutes + 60) / 180;
                this.outdated = true;
                break;

            case 19:
                if (this.state !== this.StateEnum.PM7To8) {
                    this.state = this.StateEnum.PM7To8;
                    this.raysColor = null;
                    this.raysFrequency = 0;  // Base frequency, lowers with time
                    this.bgImg = ImageManager.images['BG_sunset_sunrise.png'];
                    this.bgPosY = 0;
				    this.cloudsColorRGB = [142, 73, 120]; //"#8e4978";
                    this.bgMImg = ImageManager.images['BG_night.png'];
                    this.bgMPosY = 0;
				    this.cloudsMColorRGB = [53, 52, 84]; //"#353454";
                    this.is_bgNight = false;
                }
                this.floorMaskOpacity = (minutes + 120) / 180;
                this.outdated = true;
                break;
            default:
                this.floorMaskOpacity = 0;
			}
            
            // Setting clouds color up
            r = this.mask ? Math.floor((
                this.cloudsColorRGB[0] +
                (this.cloudsMColorRGB[0] - this.cloudsColorRGB[0]) * (minutes / 60)
            )).toString(16) : this.cloudsColorRGB[0].toString(16);
            g = this.mask ? Math.floor((
                this.cloudsColorRGB[1] +
                (this.cloudsMColorRGB[1] - this.cloudsColorRGB[1]) * (minutes / 60)
            )).toString(16) : this.cloudsColorRGB[1].toString(16);
            b = this.mask ? Math.floor((
                this.cloudsColorRGB[2] +
                (this.cloudsMColorRGB[2] - this.cloudsColorRGB[2]) * (minutes / 60)
            )).toString(16) : this.cloudsColorRGB[2].toString(16);
            this.cloudsColor = '#' + r + g + b;
            
            // Setting rays color
            if (this.raysColor === this.RaysEnum.YellowToWhite) {
                this.raysColor = [255, 255, Math.floor(127 + (128 * minutes / 60))];
            } else if (this.raysColor === this.RaysEnum.WhiteToYellow) {
                this.raysColor = [255, 255, Math.floor(255 - (128 * minutes / 60))];
            }
            
            // Setting rays frequency
            if (this.state === this.StateEnum.AM5To6) {
                this.raysFrequency = Math.floor(250 - 150 * (minutes / 60.0));
            } else if (this.state === this.StateEnum.PM6To7) {
                this.raysFrequency = Math.floor(100 + 150 * (minutes / 60.0));
            }
		}
    },
    render: function () {
        'use strict';
        
		this.renderBackground();
		this.renderSets();
		this.renderFloor();
    },
    renderBackground: function () {
		'use strict';
		var ctx = this.canvas2DContext,
			minutes = this.clock.currentTime.getMinutes(),
            windowInnerWidth = this.windowInnerWidth,
            windowInnerHeight = this.windowInnerHeight;

		ctx.drawImage(this.bgImg, 0, this.bgPosY, 1, 480, 0, 0, windowInnerWidth, windowInnerHeight);
	
		if (this.state === this.StateEnum.night ||
                this.state === this.StateEnum.AM5To6) {
			ctx.globalCompositeOperation = "overlay";
			ctx.drawImage(ImageManager.images['BG_nightLinks.png'],
				this.starsPos, 0, 320, 480,
				0, 0, windowInnerWidth, windowInnerHeight);
			ctx.globalCompositeOperation = "source-over";
			ctx.drawImage(ImageManager.images['BG_nightStars.png'],
				this.starsPos, 0, 320, 480,
				0, 0, windowInnerWidth, windowInnerHeight);
		}
		if (this.mask) {
			ctx.globalAlpha = minutes / 60.0;
			ctx.drawImage(this.bgMImg,
				0, this.bgMPosY, 1, 480,
				0, 0, windowInnerWidth, windowInnerHeight);
	
			if (this.state === this.StateEnum.PM7To8) {
				ctx.globalCompositeOperation = "soft-light";
				ctx.drawImage(ImageManager.images['BG_nightLinks.png'],
					this.starsPos, 0, 320, 480,
					0, 0, windowInnerWidth, windowInnerHeight);
				ctx.globalCompositeOperation = "source-over";
				ctx.drawImage(ImageManager.images['BG_nightStars.png'],
					this.starsPos, 0, 320, 480,
					0, 0, windowInnerWidth, windowInnerHeight);
			}
            ctx.globalAlpha = 1;
		}
	},
	renderSets: function () {
		'use strict';
		var ctx = this.canvas2DContext,
            floorHeight = this.floorImage.height,
            setsImage = this.setsImage,
            nightSetsImage = this.nightSetsImage,
            setsWidth = setsImage.width,
            setsHeight = setsImage.height,
            hRatio = this.windowHRatio,
            vRatio = this.windowVRatio;

        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
		ctx.drawImage(
            (this.is_bgNight) ? nightSetsImage : setsImage,
            0,
            0,
            setsWidth,
            setsHeight,
            window.innerWidth - (setsWidth * 3 * hRatio),
            window.innerHeight - ((setsHeight + floorHeight - 20) * 3 * hRatio),
            setsWidth * 3 * hRatio,
            setsHeight * 3 * hRatio
        );
		if (this.mask) {
			ctx.globalAlpha = this.floorMaskOpacity;
			ctx.drawImage(
                (this.is_bgNight) ? setsImage : nightSetsImage,
                0,
                0,
                setsWidth,
                setsHeight,
                window.innerWidth - (setsWidth * 3 * hRatio),
                window.innerHeight - ((setsHeight + floorHeight - 20) * 3 * hRatio),
                setsWidth * 3 * hRatio,
                setsHeight * 3 * hRatio
            );
			ctx.globalAlpha = 1;
		}
	},
	renderFloor: function () {
		'use strict';
		var ctx = this.canvas2DContext,
            nightFloorImage = this.nightFloorImage,
            floorImage = this.floorImage,
            floorWidth = floorImage.width,
            floorHeight = floorImage.height,
            hRatio = this.windowHRatio;

        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
		ctx.drawImage(
            (this.is_bgNight) ? nightFloorImage : floorImage,
            0,
            0,
            floorWidth,
            floorHeight,
            window.innerWidth - (floorWidth * 3 * hRatio),
            window.innerHeight - (floorHeight * 3 * hRatio),
            floorWidth * 3 * hRatio,
            floorHeight * 3 * hRatio
        );
        
		if (this.mask) {
			ctx.globalAlpha = this.floorMaskOpacity;
			ctx.drawImage(
                (this.is_bgNight) ? floorImage : nightFloorImage,
                0,
                0,
                floorWidth,
                floorHeight,
                window.innerWidth - (floorWidth * 3 * hRatio),
                window.innerHeight - (floorHeight * 3 * hRatio),
                floorWidth * 3 * hRatio,
                floorHeight * 3 * hRatio
            );
			ctx.globalAlpha = 1;
		}
	}
};
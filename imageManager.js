var ImageManager = {
    count: 0,
    allImagesLoaded: new Event('allImagesLoaded'),
    images: [],

    imagesFolderLocation: "./FEZ_resources/images/",

    init: function () {
        'use strict';

        //Environment Images
        this.addImage('BG_midday.png');
        this.addImage('BG_morning_end.png');
        this.addImage('BG_morning_evening.png');
        this.addImage('BG_sunset_sunrise.png');
        this.addImage('BG_night.png');
        this.addImage('BG_nightLinks.png');
        this.addImage('BG_nightStars.png');

        //Sets Imagesbgtd
        this.addImage('floor.png');
        this.addImage('floor_night.png');
        this.addImage('sets.png');
        this.addImage('sets_night.png');

        //Gomez Images
        this.addImage('gomez_up.png');
        this.addImage('gomez_sleeping.png');
        this.addImage('gomez_jumping.png');
        this.addImage('gomez_playing.png');
        this.addImage('gomez_lookingAround.png');
        this.addImage('gomez_yawning.png');
        this.addImage('gomez_blinking.png');
        this.addImage('gomez_countingSheeps.png');
        this.addImage('gomez_wakingUp.png');
        this.addImage('gomez_cube.png');

        //3D Assets Images
        this.addImage('cubeShardFace.png');
        this.addImage('faceExt.png');
        this.addImage('faceInt.png');
    },

    addImage: function (imageName) {
        'use strict';
        if (this.images[imageName] !== undefined) {
            return;
        }
        this.images.length += 1;
        this.images[imageName] = new Image();
        this.images[imageName].onload = this.imageLoaded.bind(this);
        this.images[imageName].src = this.imagesFolderLocation + imageName;
    },

    imageLoaded: function () {
        'use strict';
        this.count += 1;
        if (this.count === this.images.length) {
            document.dispatchEvent(this.allImagesLoaded);
        }
    }
};

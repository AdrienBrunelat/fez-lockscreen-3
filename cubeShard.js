var Engine3D = Engine3D || {},
    Config = Config || {},
    Ratio = Ratio || {},
    ImageManager = ImageManager || {};

var CubeShard = {
    window: window,
    cubeScale: Config.cubeScale,
    windowHRatio: undefined,

    rotationAxis: undefined,
    rotationAngle: undefined,

    offsets: [],
    isMovingUp: true,
    amp: undefined,
    x0: undefined,
    y0: undefined,
    dt: 0,
    frame: 0,
    rotFrame: undefined,
    rotTime: 0.3,
    duration: 1,  // Animation duration in seconds (up and down)

    init: function () {
        'use strict';
        var cubeScale = this.cubeScale,
            hRatio = Ratio.horizontal,
            images = ImageManager.images,
            floorHeight = images['floor.png'].height,
            setsHeight = images['sets.png'].height,
            setsWidth = images['sets.png'].width;

        // this.x0 = cubeScale * (this.window.innerWidth - (setsWidth - 15) * 3 * hRatio);
        // this.y0 = cubeScale * (this.window.innerHeight - (floorHeight + setsHeight - 60) * 3 * hRatio);

        this.x0 = cubeScale * 36 * hRatio;
        this.y0 = cubeScale * 36 * hRatio;

        Engine3D.Cube.call(
            this,
            -cubeScale * 16 * hRatio,
            -cubeScale * 16 * hRatio,
            -cubeScale * 16 * hRatio,
            cubeScale * 32 * hRatio,
            cubeScale * 32 * hRatio,
            cubeScale * 32 * hRatio
        );
        this.rotationAxis = [1, 1, 1];
        this.translation = [this.x0, this.y0, 0];

        this.amp = cubeScale * 8 * hRatio;
        this.windowHRatio = hRatio;
        this.textureImage = images['cubeShardFace.png'];

        this.preCalculateAnimation();
        this.window.setInterval(this.launchRotation.bind(this), 3000);
    },
    preCalculateAnimation: function () {
        'use strict';
        var step = 1.0 / ((this.duration / 2.0) * Config.fps),
            i;

        function getYBezier(x1, y1, x2, y2, t) {
            var Cx = 3 * x1,
                Bx = 3 * (x2 - x1) - Cx,
                Ax = 1 - Cx - Bx,

                Cy = 3 * y1,
                By = 3 * (y2 - y1) - Cy,
                Ay = 1 - Cy - By;

            function bezier_x(t) {
                return t * (Cx + t * (Bx + t * Ax));
            }

            function bezier_y(t) {
                return t * (Cy + t * (By + t * Ay));
            }

            function bezier_x_der(x) {
                return Cx + t * (2 * Bx + t * 3 * Ax);
            }

            function find_x_for(t) {
                var x = t,
                    i = 0,
                    z;

                while (i < 5) {
                    z = bezier_x(x) - t;
                    if (Math.abs(z) < 1e-3) {
                        break;
                    }

                    x = x - z / bezier_x_der(x);
                    i += 1;
                }

                return x;
            }

            return bezier_y(find_x_for(t));
        }

        this.offsets[0] = 0;
        for (i = step; i <= 1 + step; i += step) {
            this.offsets.push(getYBezier(0.42, 0, 0.58, 1, i));
        }
    },
    launchRotation: function () {
        'use strict';
        this.rotFrame = 0;
    },

    update: function (dt) {
        'use strict';
        var n = Math.floor((this.dt + dt) / (1000.0 / Config.fps)),
            len;
        //let n be the number of frames that should have been drawn in a perfect world
        //If n > 1, there is some frame skip to be made

        if (n === 0) {
            this.dt += dt;
            return;
        } else {
            this.dt = 0;
            this.outdated = true;
        }

        // Cube Translation
        this.frame += n;

        len = this.offsets.length;
        if (this.frame >= len) {
            this.frame = 1;
            this.isMovingUp = !this.isMovingUp;
        }

        if (this.isMovingUp) {
            this.translation[1] = this.y0 + this.amp * this.offsets[len - this.frame];
        } else {
            this.translation[1] = this.y0 + this.amp * this.offsets[this.frame];
        }

        // Cube Rotation
        if (this.rotFrame < Config.fps * this.rotTime) {
            this.rotFrame += n;
            if (this.rotFrame > Config.fps * this.rotTime) {
                this.rotFrame = Config.fps * this.rotTime;
            }
            this.rotationAngle += n * Engine3D.Math.degToRad(
                    120.0 / (Config.fps * this.rotTime)
                );
            if (this.rotationAngle > 2.0943951) {
                this.rotationAngle = 2.0943951;  // 120° in radians is ~2.0943951
            }
        } else {
            this.rotFrame = Config.fps * this.rotTime;
            this.rotationAngle = 0;
        }
    }
};
var Object_3D = function (origin) {
    'use strict';
    
	this.origin = origin;
	this.nodes = undefined;
	this.faces = undefined;
};

Object_3D.prototype = {
	translate: function (x, y, z) {
        'use strict';
        var i,
            len,
            node;
        
		this.origin[0] += x;
		this.origin[1] += y;
		this.origin[2] += z;

        len = this.nodes.length;
		for (i = len; i--;) {
            node = this.nodes[i];
			this.nodes[i] = [node[0] + x,
			                 node[1] + y,
			                 node[2] + z];
        }
	},
    
	translateY: function (d) {
        'use strict';
        var i,
            len;
        
		this.origin[1] += d;

        len = this.nodes.length;
		for (i = len; i--;) {
			this.nodes[i] = [this.nodes[i][0],
			                 this.nodes[i][1] + d,
			                 this.nodes[i][2]];
            //this.nodes[i][1] += d;
            //console.log(this.nodes[i][1]);
        }
	},

	//Normalizes the vector u to have Ux^2 + Uy^2 + Uz^3 = 1
	normVect: function (u) {
        'use strict';
        var math = Math,
            pow = math.pow,
            q = math.sqrt(pow(u[0], 2) + pow(u[1], 2) + pow(u[2], 2));

        return [u[0] / q, u[1] / q, u[2] / q];
    },

	//Rotates along the vector u with the angle theta
	rotateU: function (u, theta) {
        'use strict';
		var ux, uy, uz,
			x, y, z,
			x0 = this.origin[0],
			y0 = this.origin[1],
			z0 = this.origin[2],
            x1, y1, z1,
            
            math = Math,
            cos = math.cos,
            sin = math.sin,
            pow = math.pow,
            c,
            s,
            
            m_11, m_12, m_13,
            m_21, m_22, m_23,
            m_31, m_32, m_33,
        
            node,
            i,
            len;
        
		u = this.normVect(u);
		theta *= math.PI / 180.0;
        c = cos(theta);
        s = sin(theta);
        
		ux = u[0];
        uy = u[1];
        uz = u[2];
        
        m_11 = c + pow(ux, 2) * (1 - c);
        m_12 = uy * ux * (1 - c) + uz * s;
        m_13 = uz * ux * (1 - c) - uy * s;
        m_21 = ux * uy * (1 - c) - uz * s;
        m_22 = c + pow(uy, 2) * (1 - c);
        m_23 = uz * uy * (1 - c) + ux * s;
        m_31 = ux * uz * (1 - c) + uy * s;
        m_32 = uy * uz * (1 - c) - ux * s;
        m_33 = c + pow(uz, 2) * (1 - c);

        len = this.nodes.length;
		for (i = len; i--;) {
            node = this.nodes[i];
			x = node[0] - x0;
			y = node[1] - y0;
			z = node[2] - z0;

			x1 = x * m_11 +
                y * m_12 +
                z * m_13;
            
			y1 = x * m_21 +
                y * m_22 +
				z * m_23;
            
			z1 = x * m_31 +
				y * m_32 +
				z * m_33;

			this.nodes[i] = [x1 + x0, y1 + y0, z1 + z0];
		}
	},

	rotateX: function (theta) {
        'use strict';
		var x,
            y,
            z,
			x0 = this.origin[0],
			y0 = this.origin[1],
			z0 = this.origin[2],
            node,
            
            len,
            i;

        len = this.nodes.length;
		for (i = len; i--;) {
            node = this.nodes[i];
			x = node[0];
			y = node[1];
			z = node[2];

			this.nodes[i] = [x,
                             (y - y0) * Math.cos(theta) + (z - z0) * Math.sin(theta) + y0,
			                 -(y - y0) * Math.sin(theta) + (z - z0) * Math.cos(theta) + z0];
		}
	},
	rotateY: function (theta) {
        'use strict';
		var x,
            y,
            z,
			x0 = this.origin[0],
			y0 = this.origin[1],
			z0 = this.origin[2],
            node,
            len,
            i;

        len = this.nodes.length;
		for (i = len; i--;) {
            node = this.nodes[i];
			x = node[0];
			y = node[1];
			z = node[2];

			this.nodes[i] = [(x - x0) * Math.cos(theta) - (z - z0) * Math.sin(theta) + x0,
			                 y,
			                 (x - x0) * Math.sin(theta) + (z - z0) * Math.cos(theta) + z0];
		}
	},
	rotateZ: function (theta) {
        'use strict';
		var x,
            y,
            z,
			x0 = this.origin[0],
			y0 = this.origin[1],
			z0 = this.origin[2],
            node,
            len,
            i;

        len = this.nodes.length;
		for (i = len; i--;) {
            node = this.nodes[i];
			x = node[0];
			y = node[1];
			z = node[2];

			this.nodes[i] = [(x - x0) * Math.cos(theta) + (y - y0) * Math.sin(theta) + x0,
			                 -(x - x0) * Math.sin(theta) + (y - y0) * Math.cos(theta) + y0,
			                 z];
		}
	},
	proj_2D: function (f) {
        'use strict';
		var nodes = this.nodes,
            n1 = nodes[f[0]],
			n2 = nodes[f[1]],
			n3 = nodes[f[2]],
            
            n1_x = n1[0],
            n1_y = n1[1],
            n1_z = n1[2],

			v1_x = n1_x - n2[0],
			v1_y = n1_y - n2[1],
			v1_z = n1_z - n2[2],
            
			v2_x = n1_x - n3[0],
			v2_y = n1_y - n3[1],
			v2_z = n1_z - n3[2];

		return [v1_y * v2_z - v1_z * v2_y,
			    v1_z * v2_x - v1_x * v2_z,
			    v1_x * v2_y - v1_y * v2_x];
	}
};

//By Acemond

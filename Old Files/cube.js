/* Global var calls */
var Object_3D = Object_3D || {};


var Cube = function (x, y, z, w, h, d) {
    'use strict';
    
	Object_3D.call(this, [x + w / 2, y + h / 2, z + d / 2]);
	this.nodes = [[x,     y,     z], [x,     y,     z + d],
	              [x,     y + h, z], [x,     y + h, z + d],
	              [x + w, y,     z], [x + w, y,     z + d],
	              [x + w, y + h, z], [x + w, y + h, z + d]];

	this.faces = [[0, 1, 3, 2], [1, 0, 4, 5],
	              [0, 2, 6, 4], [3, 1, 5, 7],
	              [5, 4, 6, 7], [2, 3, 7, 6]];
};

Cube.prototype = Object.create(Object_3D.prototype);

Cube.prototype.constructor = Cube;

Cube.prototype.update = function () {
    'use strict';
};

Cube.prototype.render = function () {
    'use strict';
};

//By Acemond

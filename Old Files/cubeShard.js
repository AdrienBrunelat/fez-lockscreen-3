/* Global variables definition */
var Cube = Cube || {},
    Config = Config || {};


var CubeShard = function (x, y, z, w, h, d, amp) {
    'use strict';
    
	Cube.call(this, x, y + amp / 2, z, w, h, d);
    this.initialPoints = this.nodes.slice(0);
    this.initialOrigin = this.origin.slice(0);
	this.wireframe = Config.debug;

	this.rotFrame = undefined;
	this.rotTime = 0.3;

	this.offsets = [];
	this.transFrame = 0;
	this.is_movingUp = true;
	this.amp = amp;

	this.dt = 0;
	this.outdated = true;

	this.preCalculateAnimation();
    
    this.renderCanvas = document.getElementById('cube');
    this.render2DContext = this.renderCanvas.getContext('2d');

	window.setInterval(this.launchRotation.bind(this), 3000);
};

CubeShard.prototype = Object.create(Cube.prototype);

CubeShard.prototype.constructor = CubeShard;

CubeShard.prototype.launchRotation = function () {
    'use strict';
    
	this.rotFrame = 0;
};

CubeShard.prototype.preCalculateAnimation = function () {
    'use strict';
    var step = 1.5 / (Config.fps),
        i;
    
	function getYBezier(x1, y1, x2, y2, t) {
		var Cx = 3 * x1,
            Bx = 3 * (x2 - x1) - Cx,
            Ax = 1 - Cx - Bx,

            Cy = 3 * y1,
            By = 3 * (y2 - y1) - Cy,
            Ay = 1 - Cy - By;
	
		function bezier_x(t) { return t * (Cx + t * (Bx + t * Ax)); }
		function bezier_y(t) { return t * (Cy + t * (By + t * Ay)); }
		
		function bezier_x_der(x) { return Cx + t * (2 * Bx + t * 3 * Ax); }
	
		function find_x_for(t) {
			var x = t,
                i = 0,
                z;
	
			while (i < 5) {
				z = bezier_x(x) - t;
				if (Math.abs(z) < 1e-3) {
                    break;
                }

				x = x - z / bezier_x_der(x);
				i += 1;
			}

			return x;
		}
	
		return bezier_y(find_x_for(t));
	}

	this.offsets[0] = 0;
	for (i = step; i <= 1 + step; i += step) {
		this.offsets.push(getYBezier(0.42, 0, 0.58, 1, i));
	}
};

CubeShard.prototype.setDefaultPos = function () {
	'use strict';
    this.initialPoints = this.nodes.slice(0);
    this.initialOrigin = this.origin.slice(0);
};

CubeShard.prototype.resetPos = function () {
	'use strict';
    this.nodes = this.initialPoints.slice(0);
    this.origin = this.initialOrigin.slice(0);
};

CubeShard.prototype.update = function (dt) {
	'use strict';
	var n = Math.floor((this.dt + dt) / (1000.0 / Config.fps)),
        len;
    //let n be the number of frames that should have been drawn in a perfect world
    //If n > 1, there is some frame skip to be made

	if (n === 0) {
        this.dt += dt;
        return;
    } else {
        this.dt = 0;
        this.outdated = true;
    }
    
    this.resetPos();

    // Cube Translation
    this.transFrame += n;
    len = this.offsets.length;
    if (this.transFrame >= len) {
        this.transFrame = 1;
        this.is_movingUp = !this.is_movingUp;
    }

    if (this.is_movingUp) {
        this.translateY(this.amp * this.offsets[len - this.transFrame]);
    } else {
        this.translateY(this.amp * this.offsets[this.transFrame]);
    }

    // Cube Rotation
    if (this.rotFrame < Config.fps * this.rotTime) {
        this.rotFrame += n;
        if (this.rotFrame > Config.fps * this.rotTime) {
            this.rotFrame = Config.fps * this.rotTime;
        }
        this.rotateU([1, 1, 1], this.rotFrame * (120.0 / (Config.fps * this.rotTime)));
    } else {
        this.rotFrame = Config.fps * this.rotTime;
        this.rotateU([1, 1, 1], this.rotFrame * (120.0 / (Config.fps * this.rotTime)));
    }
};

CubeShard.prototype.render2 = function () {
    'use strict';
    //TODO
    
	//Make sure there's something to draw
	if (!this.outdated) {
        return;
    } else {
        this.outdated = false;
    }
};

CubeShard.prototype.render = function () {
    'use strict';
    
	//Make sure there's something to draw
	if (!this.outdated) {
        return;
    } else {
        this.outdated = false;
    }

	var render2DContext,
        len,
        fproj,
        face,
        scale = Config.cubeScale,
        i;

    render2DContext = this.render2DContext;
    
	function drawCrossHair(x, y) {
        var scale = Config.cubeScale;
        
        render2DContext.beginPath();
        render2DContext.moveTo(x - 5 * scale, y);
        render2DContext.lineTo(x + 5 * scale, y);
        render2DContext.lineTo(x, y);
        render2DContext.lineTo(x, y + 5 * scale);
        render2DContext.lineTo(x, y - 5 * scale);
        render2DContext.stroke();
    }

    render2DContext.clearRect(0, 0, this.renderCanvas.width, this.renderCanvas.height);
    render2DContext.fillStyle = '#ffd823';
    
    //Cube Colors
    //color2 = '#fde76f';
    //color1 = '#ffd823';

    if (scale < 2) {
        render2DContext.translate(scale * 0.5, scale * 0.5);
    }
    
    len = this.faces.length;
    for (i = len; i --;) {
        face = this.faces[i];
		fproj = this.proj_2D(face);
		if (this.wireframe || fproj[2] > 0) {
			render2DContext.strokeStyle = this.wireframe ? 'grey' : '#fde76f';
            render2DContext.lineWidth = scale;
			render2DContext.beginPath();
			render2DContext.moveTo(this.nodes[face[0]][0], this.nodes[face[0]][1]);
			render2DContext.lineTo(this.nodes[face[1]][0], this.nodes[face[1]][1]);
			render2DContext.lineTo(this.nodes[face[2]][0], this.nodes[face[2]][1]);
			render2DContext.lineTo(this.nodes[face[3]][0], this.nodes[face[3]][1]);
			render2DContext.closePath();
			render2DContext.fillStyle = '#ffd823';
			if (!this.wireframe) {
                render2DContext.fill();
            }
			render2DContext.stroke();
			if (this.wireframe) {
                render2DContext.strokeStyle = 'green';
                drawCrossHair(this.origin[0], this.origin[1]);
			    render2DContext.strokeStyle = 'black';
                drawCrossHair(this.nodes[face[0]][0], this.nodes[face[0]][1]);
                drawCrossHair(this.nodes[face[1]][0], this.nodes[face[1]][1]);
                drawCrossHair(this.nodes[face[2]][0], this.nodes[face[2]][1]);
                drawCrossHair(this.nodes[face[3]][0], this.nodes[face[3]][1]);
            }
		}
    }
    
    if (scale < 2) {
        render2DContext.translate(scale * -0.5, scale * -0.5);
    }
};

var Engine3D = Engine3D || {};

// Defines a 3D Plane
// x0: X coordinate of the origin
// y0: Y coordinate of the origin
// z0: Z coordinate of the origin
// w: Width of the plane
// h: Height of the plane
// u: Normal vector of the plane
Engine3D.Plane = function (x0, y0, z0, w, h, u) {
    'use strict';
    
    Engine3D.Object3D.call(this, [x0, y0, z0]);
    
    // TODO
    
    this.texture = undefined;
};

Engine3D.Plane.prototype = {
    constructor: Engine3D.Plane
};
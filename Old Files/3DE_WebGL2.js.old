var console = console || {},
    Float32Array = Float32Array || {},
    Engine3D = Engine3D || {};

Engine3D.WebGLRenderer = function (canvas) {
    'use strict';
    
    var gl,
        program,
        positionLocation,
        colorLocation,
        texCoordLocation,
        matrixLocation,
        canvasWidth = canvas.width,
        canvasHeight = canvas.height,
        then = 0,
        translation = [0, 0, 0],
        rotation = [0, 0, 0],
        rotationU = [1, 1, 1, 0],
        uAngle = 0,
        sceneVertices = 0;
        
    function initGL(canvas) {
        try {
            gl = canvas.getContext('webgl') ||
                canvas.getContext('experimental-webgl');
        } catch (e) {
            console.err('3DEngine.WebGLRenderer: ' + e);
        }
        if (!gl) {
            console.warn("Couldn't initialize WebGL!");
        }
    }
    function compileShader(shaderSource, shaderType) {
        var shader = gl.createShader(shaderType);

        gl.shaderSource(shader, shaderSource);
        gl.compileShader(shader);

        return shader;
    }
    function initShaders() {
        var vertexShaderSource,
            framgmentShaderSource,
            vertexShader,
            fragmentShader;
        
        vertexShaderSource = document.getElementById('3d-vertex-shader').textContent;
        framgmentShaderSource = document.getElementById('3d-texture-fragment-shader').textContent;
        
        vertexShader = compileShader(vertexShaderSource, gl.VERTEX_SHADER);
        fragmentShader = compileShader(framgmentShaderSource, gl.FRAGMENT_SHADER);
        
        // Create the shader program
        program = gl.createProgram();
        gl.attachShader(program, vertexShader);
        gl.attachShader(program, fragmentShader);
        gl.linkProgram(program);
        
        gl.useProgram(program);
    }
    function setOptions() {
        //gl.enable(gl.CULL_FACE);
        gl.enable(gl.DEPTH_TEST);
    }
    function setGeometry(object) {
        var len,
            i,
            node,
            objectNodes = object.nodes,
            objectVertices = object.vertices,
            vertices = [];
        
        len = objectVertices.length;
        for (i = 0; i < len; i += 1) {
            node = objectVertices[i] * 3;
            
            vertices.push(objectNodes[node]);
            vertices.push(objectNodes[node + 1]);
            vertices.push(objectNodes[node + 2]);
            
            sceneVertices += 1;
        }
        
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    }
    function setTexture(object) {
        var len,
            i,
            vertices = [];
        
        len = object.vertices.length;
        for (i = 0; i < len; i += 1) {
            vertices = vertices.concat(Engine3D.Math.apply2DRotation([
                0, 0,
                0, 1,
                1, 0,
                0, 1,
                1, 1,
                1, 0
            ], Engine3D.Math.degToRad(object.textureOrientation[i])));
        }
        
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    }
    function drawScene(now) {
        var deltaTime,
            matrix,
            triangles,
            
            translationMatrix,
            rotationXMatrix,
            rotationYMatrix,
            rotationZMatrix,
            rotationUMatrix,
            projectionMatrix;
        
        now *= 0.001;
        deltaTime = now - then;
        then = now;
        
        rotationU[3] += deltaTime;
        
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        
        translationMatrix = Engine3D.Math.translationMatrix(
            translation[0],
            translation[1],
            translation[2]
        );
        rotationXMatrix = Engine3D.Math.rotationXMatrix(rotation[0]);
        rotationYMatrix = Engine3D.Math.rotationYMatrix(rotation[1]);
        rotationZMatrix = Engine3D.Math.rotationZMatrix(rotation[2]);
        rotationUMatrix = Engine3D.Math.rotationUMatrix(
            rotationU[0],
            rotationU[1],
            rotationU[2],
            rotationU[3]
        );
        projectionMatrix = Engine3D.Math.projectionMatrix(
            canvasWidth,
            canvasHeight,
            canvasWidth * 2
        );
        
        matrix = rotationZMatrix;
        matrix = Engine3D.Math.multiplyMatrices(matrix, rotationYMatrix);
        matrix = Engine3D.Math.multiplyMatrices(matrix, rotationXMatrix);
        matrix = Engine3D.Math.multiplyMatrices(matrix, rotationUMatrix);
        
        matrix = Engine3D.Math.multiplyMatrices(matrix, translationMatrix);
        matrix = Engine3D.Math.multiplyMatrices(matrix, projectionMatrix);
        
        gl.uniformMatrix4fv(matrixLocation, false, matrix);
        gl.drawArrays(gl.TRIANGLES, 0, sceneVertices);
        requestAnimationFrame(drawScene);
    }
    
    // WebGL Start
    initGL(canvas);
    initShaders();
    
    setOptions();
    
    positionLocation = gl.getAttribLocation(program, "a_position");
    texCoordLocation = gl.getAttribLocation(program, "a_texCoord");
    
    matrixLocation = gl.getUniformLocation(program, "u_matrix");
    
    gl.enableVertexAttribArray(positionLocation);
    gl.enableVertexAttribArray(texCoordLocation);
    
    requestAnimationFrame(drawScene);
    
    this.addObject = function addObject(object) {
        var buffer,
            texture;
        
        buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.vertexAttribPointer(positionLocation, 3, gl.FLOAT, false, 0, 0);

        setGeometry(object);
        
        buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.vertexAttribPointer(texCoordLocation, 2, gl.FLOAT, false, 0, 0);
        
        setTexture(object);
        
        texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

        gl.texImage2D(
            gl.TEXTURE_2D,
            0,
            gl.RGBA,
            gl.RGBA,
            gl.UNSIGNED_BYTE,
            object.textureImage
        );
        
        translation = object.translation;
    }
};
/* Global var calls */
var Ratio = Ratio || {},
    Environment = Environment || {},
    ImageManager = ImageManager || {};


var CloudsRenderer = {
    cloudBumpsCanvas: undefined,
    cloudAnimPos: [0, 0, 0],
    cloudFrame: 0,
    newCloudsCanvas: undefined,
    newClouds2DContext: undefined,
    tempCloudCanvas: undefined,
    tempCloud2DContext: undefined,
    drawingCanvas: undefined,
    drawing2DContext: undefined,
    window: window,
    innerWidth: this.window.innerWidth,
    innerHeight: this.window.innerHeight,
    outdated: true,
    framePeriod: Math.floor(20 / Config.cloudsSpeed),
    
    init: function () {
        'use strict';
        var cloudsSpace = 160,
            wCloud0,
            wCloud1,
            wHole0,
            wHole1,
            holesSpace,
            cloudHeight = [60, 130, 170],
            cloudBumpsHeight = [46, 24, 16],
            newCloudBumpsCanvas,
            newCloudBumps2DContext,
            tempCloudCanvas,
            newCloudsCanvas,
            
            floor = Math.floor,
            random = Math.random,
            hRatio = Ratio.horizontal,
            vRatio = Ratio.vertical,
            n;

        this.cloudBumpsCanvas = [undefined, undefined, undefined];

        //for (n = 0; n < 3; n += 1) {
        for (n = 3; n--;) {
            wCloud0 = floor((cloudsSpace - 142) * random()) + 42;
            wCloud1 = floor((cloudsSpace - wCloud0 - 100) * random()) + 34;

            holesSpace = 160 - (wCloud0 + wCloud1);
            wHole0 = floor((holesSpace - 66) * random()) + 22;
            wHole1 = floor((holesSpace - (wHole0 + 44)) * random()) + 22;

            wCloud0 *= 2 * hRatio;
            wCloud1 *= 2 * hRatio;

            wHole0 *= 2 * hRatio;
            wHole1 *= 2 * hRatio;

            newCloudBumpsCanvas = document.createElement("canvas");
            newCloudBumpsCanvas.width = 2 * this.innerWidth;
            newCloudBumpsCanvas.height = cloudBumpsHeight[n] * vRatio;
            this.cloudBumpsCanvas[n] = newCloudBumpsCanvas;

            newCloudBumps2DContext = newCloudBumpsCanvas.getContext('2d');
            newCloudBumps2DContext.fillStyle = 'white';
            this.drawCloudBumps(newCloudBumps2DContext, wHole0, wCloud0, cloudBumpsHeight[n] * vRatio, 3 - n);
            this.drawCloudBumps(newCloudBumps2DContext, wHole0 + wCloud0 + wHole1, wCloud1, cloudBumpsHeight[n] * vRatio, 3 - n);

            newCloudBumps2DContext.drawImage(newCloudBumpsCanvas, newCloudBumpsCanvas.width / 2, 0);

            this.cloudAnimPos[n] = floor(this.innerWidth * random());
            this.drawingCanvas = document.getElementById('clouds');
            this.drawing2DContext = this.drawingCanvas.getContext('2d');
        }

        //Initialize cache canvases used for animation
        tempCloudCanvas = document.createElement("canvas");
        tempCloudCanvas.width = this.innerWidth;
        tempCloudCanvas.height = this.innerHeight;
        this.tempCloudCanvas = tempCloudCanvas;
        this.tempCloud2DContext = tempCloudCanvas.getContext('2d');

        newCloudsCanvas = document.createElement("canvas");
        newCloudsCanvas.width = this.innerWidth;
        newCloudsCanvas.height = (cloudHeight[2] + this.cloudBumpsCanvas[2].height) * vRatio;
        this.newCloudsCanvas = newCloudsCanvas;
        this.newClouds2DContext = newCloudsCanvas.getContext('2d');
        
        Environment.cloudsRenderer = this;
    },
    
    drawCloudBumps: function (ctx, x, w, h, hBump, noBumpIn, noBumpOut) {
		'use strict';
        var i;
	
		if (noBumpIn) {
			ctx.fillRect(x, 0, 6 * hBump, h);
        } else {
			for (i = hBump; i--;) {
				ctx.fillRect(x + 3 * i, 0, 3, 3 * (i + 1));
				ctx.fillRect(x + 3 * (i + hBump), 0, 3, h - 3 * (hBump - i));
			}
		}
		ctx.fillRect(x + 6 * hBump, 0, w - 12 * hBump, h);
		
		if (noBumpOut) {
			ctx.fillRect(x + w - 6 * hBump, 0, 6 * hBump, h);
        } else {
			for (i = hBump; i--;) {
				ctx.fillRect(x + w - 6 * hBump + 3 * i, 0, 3, h - 3 * (i + 1));
				ctx.fillRect(x + w - 6 * hBump + 3 * (i + hBump), 0, 3, 3 * (hBump - i));
			}
		}
	},
	
	update: function () {
		'use strict';
		var cloudHeight = [60, 130, 170],
			cloudOpacity = [1, 0.375, 0.175],
			minutes = new Date().getMinutes(),
            
            tempCloudCanvas,
            tempCloud2DContext,
            newCloudsCanvas,
            newClouds2DContext,
            cloudBumpsCanvas,
            cloudBumpsCanvasHeight,
            drawing2DContext,
            
            vRatio = Ratio.vertical,
            environment = Environment,
            
            n;
	
        if (!this.outdated) {
            if (!Config.movingClouds || this.cloudFrame % this.framePeriod !== 0) {
                return;
            }
        }
        this.outdated = true;
	
        tempCloudCanvas = this.tempCloudCanvas;
		tempCloud2DContext = this.tempCloud2DContext;
		tempCloud2DContext.clearRect(0, 0, tempCloudCanvas.width, tempCloudCanvas.height);
	
        newCloudsCanvas = this.newCloudsCanvas;
        newClouds2DContext = this.newClouds2DContext;
		newClouds2DContext.clearRect(0, 0, newCloudsCanvas.width, newCloudsCanvas.height);
		for (n = 3; n--;) {
            cloudBumpsCanvas = this.cloudBumpsCanvas[n];
            cloudBumpsCanvasHeight = cloudBumpsCanvas.height;
			newClouds2DContext.drawImage(cloudBumpsCanvas,
                                             this.cloudAnimPos[n], 0,
                                             this.innerWidth, cloudBumpsCanvasHeight,
                                             0, cloudHeight[n] * vRatio,
                                             this.innerWidth, cloudBumpsCanvasHeight
                                        );
	
			tempCloud2DContext.fillStyle = environment.cloudsColor;
			tempCloud2DContext.globalAlpha = environment.mask ? cloudOpacity[n] * (1 - (minutes / 60)) : cloudOpacity[n];
			if (Config.debug) {
                tempCloud2DContext.fillStyle = '#' +
                    (n === 0 ? 'ff' : '00') +
                    (n === 1 ? 'ff' : '00') +
                    (n === 2 ? 'ff' : '00');
                tempCloud2DContext.globalAlpha = 1;
            }
			tempCloud2DContext.fillRect(0, cloudHeight[n] * vRatio, this.innerWidth, cloudBumpsCanvasHeight);
			if (environment.mask) {
				tempCloud2DContext.fillStyle = environment.cloudsMColor;
                if (Config.debug) {
                    tempCloud2DContext.fillStyle = '#' +
                        (n !== 0 ? 'ff' : '00') +
                        (n !== 1 ? 'ff' : '00') +
                        (n !== 2 ? 'ff' : '00');
                }
				tempCloud2DContext.globalAlpha = cloudOpacity[n] * (minutes / 60);
				tempCloud2DContext.fillRect(0, cloudHeight[n] * vRatio, this.innerWidth, cloudBumpsCanvasHeight);
			}
			tempCloud2DContext.globalAlpha = 1;
		}
		tempCloud2DContext.globalCompositeOperation = "destination-in";
		tempCloud2DContext.drawImage(newCloudsCanvas, 0, 0);
		tempCloud2DContext.globalCompositeOperation = "source-over";
	},
	render: function () {
		'use strict';
		var drawing2DContext,
            n;
        
        if (!this.outdated && this.cloudFrame % this.framePeriod !== 0) {
            if (Config.movingClouds) {
                this.cloudFrame += 1;
            }
            return;
        }
	    this.outdated = false;
        
        drawing2DContext = this.drawing2DContext;
		drawing2DContext.webkitImageSmoothingEnabled = false;
		drawing2DContext.clearRect(0, 0, this.drawingCanvas.width, this.drawingCanvas.height);
		this.renderSetsMask();
		drawing2DContext.globalCompositeOperation = 'source-out';
		drawing2DContext.drawImage(this.tempCloudCanvas, 0, 0);
		drawing2DContext.globalCompositeOperation = 'source-over';
	
		//If the environment turned into outdated mode while cloudFrame was not a mult of 20
		//That means it was an environment color change special frame
		//The clouds should not move in this case and only be updated in terms of color
		if (!Config.movingClouds || (this.outdated && this.cloudFrame % this.framePeriod !== 0)) {
            return;
        }

		for (n = 3; n--;) {
            if ((this.cloudFrame + this.framePeriod) %
                (this.framePeriod * (n + 1)) === 0) {
                this.cloudAnimPos[n] += 1;
            }
			if (this.cloudAnimPos[n] >= this.cloudBumpsCanvas[n].width / 2) {
                this.cloudAnimPos[n] = 0;
            }
		}
		if (this.cloudFrame >= 3 * this.framePeriod) {
            this.cloudFrame = 0;
        }
	
		this.cloudFrame += 1;
	},
	renderSetsMask: function () {
		'use strict';
		var setsImage = ImageManager.images['sets.png'],
            setsWidth = setsImage.width,
            setsHeight = setsImage.height,
            hRatio = Ratio.horizontal,
            x = this.innerWidth - (setsWidth * 3 * hRatio),
			y = this.innerHeight - ((ImageManager.images['floor.png'].height + setsHeight - 20) * 3 * hRatio);
	
		this.drawing2DContext.webkitImageSmoothingEnabled = false;
		this.drawing2DContext.drawImage(setsImage,
                          0, 0,
                          setsWidth, setsHeight,
                          x, y,
                          setsWidth * 3 * hRatio, setsHeight * 3 * hRatio
                     );
	}
};

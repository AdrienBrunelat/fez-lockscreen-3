/* Global var calls */
var Config = Config || {},
    Environment = Environment || {};

var Clock = {
    currentTime: new Date(),
    lastTime: undefined,
    timeSeed: undefined,

    init: function () {
        'use strict';

        var currentTime = this.currentTime;

        Environment.clock = this;
        this.lastTime = currentTime;
        this.timeSeed = (currentTime.getDay() + currentTime.getMonth()) * (currentTime.getDate() + currentTime.getYear());
        this.updateClock();
        setTimeout(this.checkTime.bind(this), Config.clockCheckInterval);
    },

    setClockBack: function () {
        'use strict';

        window.timeDate.className = 'defaultClockState';

        //window.timeDate.style.opacity = 1;
        //window.timeDate.style.transform = 'translateY(0px)';
        //$("#timeDate").css('-webkit-transform', 'translateY(0px)');
        //window.timeDate.style.webkitFilter = '';
        //$("#timeDate").css("-webkit-filter", '');
    },

    /*Changes all texts font when clock is clicked*/
    switchTimeFont: function () {
        'use strict';
        if (Config.pixel) {
            window.timeDate.style.fontFamily = 'fezzy';
            window.slideToUnlock.style.fontFamily = 'fezzy';
            Config.pixel = false;
        } else {
            window.timeDate.style.fontFamily = 'pixel';
            window.slideToUnlock.style.fontFamily = 'pixel';
            Config.pixel = true;
        }
    },

    //Sets the right color for the clock digits and its outline
    updateClockColor: function (hours) {
        'use strict';

        if (hours > 5 && hours < 19) {
            window.timeDate.style.color = "white";
            window.timeDate.style.textShadow = "-1px -1px 0px black, -1px 1px 0px black, 1px -1px 0px black, 1px 1px 0px black";
        } else {
            window.timeDate.style.color = "black";
            window.timeDate.style.textShadow = "-1px -1px 0px white, -1px 1px 0px white, 1px -1px 0px white, 1px 1px 0px white";
        }
    },


    /*periodcally checks if clock needs an update*/
    checkTime: function () {
        'use strict';

        var currentTime,
            lastTime = this.lastTime;

        this.currentTime = new Date();
        currentTime = this.currentTime;

        if (currentTime.getMinutes() !== lastTime.getMinutes() ||
            currentTime.getHours() !== lastTime.getHours() ||
            currentTime.getDate() !== lastTime.getDate()) {
            this.lastTime = currentTime;
            this.updateClock();
        }

        setTimeout(this.checkTime.bind(this), Config.clockCheckInterval);
    },

    /*actually updates inner HTML corresponding to the clock*/
    /*Called every time that the time change (by the hours of the minutes)*/
    updateClock: function () {
        'use strict';
        var currentTime = this.currentTime,
            minutes = currentTime.getMinutes(),
            hours = currentTime.getHours(),
            day = currentTime.getDay(),
            date = currentTime.getDate(),
            month = currentTime.getMonth(),

            dispHours,
            dispMinutes,
            dispDay,
            dispMonth,

            ampmValue;

        //Environment.update();
        this.updateClockColor(hours);

        if (minutes < 10) {
            dispMinutes = "0" + minutes;
        } else {
            dispMinutes = String(minutes);
        }

        dispHours = hours;
        if (Config.mode12h) {
            ampmValue = (hours >= 12) ? "pm" : "am";
            if (hours > 12) {
                dispHours -= 12;
            }
            if (hours === 0) {
                dispHours = 12;
            }
        }

        //Here and here ONLY, hours becomes a String.
        if (dispHours < 10) {
            dispHours = "0" + dispHours;
        } else {
            dispHours = String(dispHours);
        }

        switch (day) {
            case 0:
                dispDay = "Sunday";
                break;
            case 1:
                dispDay = "Monday";
                break;
            case 2:
                dispDay = "Thusday";
                break;
            case 3:
                dispDay = "Wednesday";
                break;
            case 4:
                dispDay = "Thursday";
                break;
            case 5:
                dispDay = "Friday";
                break;
            case 6:
                dispDay = "Saturday";
                break;
        }

        switch (month) {
            case 0:
                dispMonth = "January";
                break;
            case 1:
                dispMonth = "February";
                break;
            case 2:
                dispMonth = "March";
                break;
            case 3:
                dispMonth = "April";
                break;
            case 4:
                dispMonth = "May";
                break;
            case 5:
                dispMonth = "June";
                break;
            case 6:
                dispMonth = "July";
                break;
            case 7:
                dispMonth = "August";
                break;
            case 8:
                dispMonth = "September";
                break;
            case 9:
                dispMonth = "October";
                break;
            case 10:
                dispMonth = "November";
                break;
            case 11:
                dispMonth = "December";
                break;
        }

        //hours.innerHTML = bgWidth + "x" + bgHeight + "</span>";
        if (Config.mode12h) {
            window.hours.innerHTML = dispHours + ":" + dispMinutes + "<span id='ampm'>" + ampmValue + "</span>";
        } else {
            window.hours.innerHTML = dispHours + ":" + dispMinutes;
        }

        window.date.innerHTML = dispDay + ", " + dispMonth + " " + String(date);
    }
};

//by Acemond

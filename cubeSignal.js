var Config = Config || {},
    ImageManager = ImageManager || {},
    Ratio = Ratio || {};

var Signal = {
    duration: 2.0,  // Animation duration in seconds
    fps: Config.fps,  // FPS of the signal animation
    scale: Config.signalScale,  // Drawing scale of signal
    firstLaunch: 5000,  // First signal launch (in ms)
    signalPeriod: 10000,  // Signal period (in ms)

    bezier: [],
    frame: undefined,
    canvas: undefined,
    canvas2DContext: undefined,
    signalTimeout: undefined,
    windowHRatio: undefined,
    windowInnerWidth: undefined,
    windowInnerHeight: undefined,
    opacity: undefined,
    outdated: false,
    deltaTime: 0,
    signalCenter: [0, 0],
    floorImage: undefined,
    signalPosisiton: undefined,

    init: function (canvas) {
        'use strict';
        var renderingWindow = window,
            setsImage = ImageManager.images['sets.png'],
            signal = this;

        this.frame = this.duration * this.fps + 1;
        this.floorImage = ImageManager.images['floor.png'];
        this.windowHRatio = Ratio.horizontal;
        this.windowInnerWidth = renderingWindow.innerWidth;
        this.windowInnerHeight = renderingWindow.innerHeight;

        this.signalCenter = [
            this.scale *
            (this.windowInnerWidth -
                (setsImage.width - 15)
                * 3 * this.windowHRatio
            ),
            this.scale *
            (this.windowInnerHeight -
                (this.floorImage.height + setsImage.height - 60)
                * 3 * this.windowHRatio
            )
        ];

        canvas.width = canvas.height = this.scale * this.windowInnerHeight;
        canvas.style.width = canvas.style.height = this.windowInnerHeight + 'px';
        this.canvas = canvas;
        this.canvas2DContext = canvas.getContext('2d');
        this.canvas2DContext.lineWidth = this.windowHRatio;

        this.preCalculateAnimation();

        this.signalTimeout = setTimeout(
            function () {
                signal.launch();
                renderingWindow.setInterval(
                    function () {
                        signal.launch();
                    },
                    signal.signalPeriod
                );
            },
            this.firstLaunch
        );
    },
    launch: function () {
        'use strict';
        this.frame = 0;
    },
    preCalculateAnimation: function () {
        'use strict';
        var i = 0,
            step = 1.0 / (this.duration * this.fps);

        function getYBezier(x1, y1, x2, y2, t) {
            var Cx = 3 * x1,
                Bx = 3 * (x2 - x1) - Cx,
                Ax = 1 - Cx - Bx,

                Cy = 3 * y1,
                By = 3 * (y2 - y1) - Cy,
                Ay = 1 - Cy - By;

            function bezier_x(t) {
                return t * (Cx + t * (Bx + t * Ax));
            }

            function bezier_y(t) {
                return t * (Cy + t * (By + t * Ay));
            }

            function bezier_x_der(x) {
                return Cx + t * (2 * Bx + t * 3 * Ax);
            }

            function find_x_for(t) {
                var x = t, i = 0, z;

                while (i < 5) {
                    z = bezier_x(x) - t;

                    if (Math.abs(z) < 1e-3) {
                        break;
                    }

                    x = x - z / bezier_x_der(x);
                    i += 1;
                }

                return x;
            }

            return bezier_y(find_x_for(t));
        }

        for (i = 0; i <= 1 + step; i += step) {
            this.bezier.push(getYBezier(0.1, 1, 1, 1, i));
        }
    },

    update: function (deltaTime) {
        'use strict';
        var totalFrames = this.duration * this.fps,
            n,
            b_t,
            scale,
            hRatio;

        if (this.frame > totalFrames) {
            return;
        }

        n = Math.floor((this.deltaTime + deltaTime) / (1000 / this.fps));
        scale = this.scale;
        hRatio = this.windowHRatio;

        if (n === 0) {
            this.deltaTime += deltaTime;
            return;
        } else {
            this.deltaTime = 0;
            this.frame += n;

            b_t = this.bezier[this.frame];

            this.signalPosisiton = scale * 100 * hRatio * b_t + scale * 12 * hRatio;

            if (this.frame >= totalFrames / 2.0) {
                this.opacity = (totalFrames - this.frame) / (totalFrames / 2.0);
            } else {
                this.opacity = 1.0;
            }

            this.outdated = true;
        }

    },
    drawLosange: function (ctx, t, opacity) {
        'use strict';
        var x = this.signalCenter[0],
            y = this.signalCenter[1];

        ctx.globalAlpha = opacity;
        ctx.beginPath();
        ctx.moveTo(x, y + t);
        ctx.lineTo(x + t, y);
        ctx.lineTo(x, y - t);
        ctx.lineTo(x - t, y);
        ctx.lineTo(x, y + t);
        ctx.lineWidth = Config.signalScale;
        ctx.strokeStyle = '#fff600';
        ctx.stroke();
        ctx.globalAlpha = 1;
    },
    renderFloorMask: function (ctx) {
        'use strict';
        var hRatio = this.windowHRatio,
            scale = this.scale,
            floorImage = this.floorImage,
            floorWidth = floorImage.width,
            floorHeight = floorImage.height,
            innerWidth = this.windowInnerWidth,
            innerHeight = this.windowInnerHeight;

        ctx.globalCompositeOperation = 'destination-out';
        ctx.drawImage(
            floorImage,
            0,
            0,
            floorWidth,
            floorHeight,
            scale * (innerWidth - (floorWidth * 3 * hRatio)),
            scale * (innerHeight - (floorHeight * 3 * hRatio)),
            scale * floorWidth * 3 * hRatio,
            scale * floorHeight * 3 * hRatio
        );
        ctx.globalCompositeOperation = 'source-over';
    },
    render: function () {
        'use strict';
        var ctx,
            opacity,
            signalPosition;

        if (this.outdated !== true) {
            return;
        }

        ctx = this.canvas2DContext;
        opacity = this.opacity;
        signalPosition = this.signalPosisiton;

        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.drawLosange(ctx, signalPosition, 0.25 * opacity);
        this.drawLosange(ctx, 2.0 * signalPosition, 0.50 * opacity);
        this.drawLosange(ctx, 3.0 * signalPosition, 0.75 * opacity);
        this.drawLosange(ctx, 4.0 * signalPosition, opacity);
        this.renderFloorMask(ctx);

        this.outdated = false;
    }
};